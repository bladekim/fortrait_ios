//
//  UserContext.m
//
//  Created by Bladekim on 10. 4. 13..
//  Copyright 2010 ChocolateSoft. All rights reserved.
//

#import "UserContext.h"
#import <QuartzCore/QuartzCore.h>


@implementation UserContext

//
// singleton stuff
//
static UserContext *_sharedUserContext = nil;

+ (UserContext *)sharedUserContext
{
	@synchronized(self)
	{
		if (!_sharedUserContext) {
			_sharedUserContext = [[self alloc] initWithDefault];
		}
		return _sharedUserContext;
	}
	// to avoid compiler warning
	return nil;
}

+ (id)alloc
{
	@synchronized(self)
	{
		NSAssert(_sharedUserContext == nil, @"Attempted to allocate a second instance of a singleton.");
		_sharedUserContext = [super alloc];
		return _sharedUserContext;
	}
	// to avoid compiler warning
	return nil;
}


- (id) initWithDefault
{
	self = [super init];
	if (self != nil) {
        // SNS Account 를 준비하자.
//        ACAccountStore *acStore = [[ACAccountStore alloc] init];
//        ACAccountType *acType = [acStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];

        // Facebook 으로 접근을 요구함.
//        NSDictionary *opdic = @{
//            ACFacebookAppIdKey: @"138838542797853",
//            ACFacebookPermissionGroupKey: @"write",
//            ACFacebookPermissionsKey: @[@"user_about_me", @"read_stream", @"publish_stream", @"offline_access"],
//            ACFacebookAppVersionKey:@"1.0"};
//        [acStore requestAccessToAccountsWithType:acType options:opdic completion:^(BOOL granted, NSError *err){
//            if( granted ) {
//                // 접근이 허용되면 
//                NSArray *acArray = [acStore accountsWithAccountType:acType];
//                _fAccount = [acArray lastObject];
//                NSLog(@"facebook Account:%@ token:%@",_fAccount,_fAccount.credential.oauthToken);
//            } else {
//                UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Notice" message:@"You have to allow access to facebook account" delegate:nil cancelButtonTitle:@"Confirm" otherButtonTitles:nil];
//                [av show];
//            }
//        }];
        self.itemColor = @[COLOR_0,COLOR_1,COLOR_2,COLOR_3,COLOR_4,COLOR_5,[UIColor blackColor]];
        self.itemTextGirl = @[@"Sexy",@"Cute",@"Gorgeous",@"Pretty",@"Lovely",@"Beautiful"];
        self.itemTextMan = @[@"Sexy",@"Sweet",@"Handsome",@"Cool",@"Tough",@"Strong"];
    }
	return self;
}

#pragma mark - Wating View

- (void) startWaitView: (NSInteger) yDeltaPos
{
	if( nil == waitV ) {
		UIWindow* mainWindow = [[UIApplication sharedApplication] keyWindow];
		waitV = [[WaitView alloc] initWithFrame:CGRectMake(mainWindow.frame.size.width/2-40, (mainWindow.frame.size.height/2)-40+yDeltaPos,80,80)];

        [waitV setAlpha:0.0];
        [waitV setTransform:CGAffineTransformMakeScale(1.5, 1.5)];
		[mainWindow addSubview:waitV];

        [UIView animateWithDuration:0.4 animations:^(void) {
            [waitV setTransform:CGAffineTransformMakeScale(1.0, 1.0)];
            [waitV setAlpha:1.0];
        } completion:^(BOOL finished) {
            ;
        }];
	}
}

- (void) stopWaitView
{
	if( waitV ){
        [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
        [UIView animateWithDuration:0.3 animations:^(void) {
            [waitV setAlpha:0.0];
            [waitV setTransform:CGAffineTransformMakeScale(1.5, 1.5)];
        } completion:^(BOOL finished) {
            [waitV removeFromSuperview];
        }];
		waitV = nil;
	}
}

#pragma mark - Toast views

- (void) completeView
{
    UIWindow* mainWindow = [[UIApplication sharedApplication] keyWindow];
    UIView *cV = [[UIView alloc] initWithFrame:CGRectMake(mainWindow.frame.size.width/2-40, (mainWindow.frame.size.height/2)-40,80,80)];
    [cV setBackgroundColor:CS_RGBA(0, 0, 0, 0.7)];
    [cV.layer setCornerRadius:10.0];
    [cV setClipsToBounds:YES];
    UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 26, 26)];
    [imgV setImage:[UIImage imageNamed:@"checkIcon.png"]];
    [imgV setCenter:CGPointMake(cV.bounds.size.width/2, cV.bounds.size.height/2)];
    [cV addSubview:imgV];

    [cV setAlpha:0.0];
    [cV setTransform:CGAffineTransformMakeScale(1.5, 1.5)];
    [mainWindow addSubview:cV];

    [UIView animateWithDuration:0.4 animations:^(void) {
        [cV setTransform:CGAffineTransformMakeScale(1.0, 1.0)];
        [cV setAlpha:1.0];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.8 animations:^(void) {
            [cV setAlpha:1.0];
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.4 animations:^(void) {
                [cV setTransform:CGAffineTransformMakeScale(1.5, 1.5)];
                [cV setAlpha:0.0];
            } completion:^(BOOL finished) {
                [cV removeFromSuperview];
            }];
        }];
    }];
}

// 시험적인 효과. 위치 주변에서 사바으로 별 5개가 퍼지는 애니메이션.
- (void) splashEffectPoint:(CGPoint)point atView:(UIView*) view
{
    UIWindow* mainWindow = [[UIApplication sharedApplication] keyWindow];
    CGPoint newPnt = [mainWindow convertPoint:point fromView:view];

    NSMutableArray *sa = [[NSMutableArray alloc] initWithCapacity:5];

    for( NSUInteger idx = 0; idx < 7; idx++ ) {
        NSUInteger ss = RANDOM_INT(10, 55);
        UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, ss, ss)];
        [imgV setCenter:newPnt];
		[imgV setImage:[UIImage imageNamed:@"star_s.png"]];
        [sa addObject:imgV];
        [mainWindow addSubview:imgV];
    }

    for( UIImageView *starV in sa )
    {
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView animateWithDuration:0.7 animations:^{
            NSInteger newX = RANDOM_INT(0, 100) - 50;
            NSInteger newY = RANDOM_INT(0, 100) - 50;

            [starV setFrame:CGRectOffset(starV.frame, newX, newY)];

            [starV setAlpha:0.0];
        } completion:^(BOOL finished) {
            [starV removeFromSuperview];
        }];

        CABasicAnimation* rotationAnimation;
        rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 /* full rotation*/ * RANDOM_INT(10, 30) ];
        rotationAnimation.duration = 10.0;
        rotationAnimation.cumulative = YES;
        rotationAnimation.repeatCount = 10;

        [starV.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    }
}

#pragma mark -

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
