//
//  NetInterface.h
//  facevot
//
//  Created by 김태한 on 12. 7. 14..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//

/*
 page별로 현재 20개씩 가져오도록 고정되어 있음

 전체 사진을 대상으로 최근에 추가한 사진 순서로 가져오는 API:

 HTTP GET: http://localhost:8081/photo?page=1&sex=1

 1) 내 사진 목록 가져오기
 HTTP GET http://<server>/photo/mine?page=1&identity=wonkim
 
 2) 주간 최대 투표 사진 가져오기
 HTTP GET
 http://localhost:8081/vote/weekly?page=1
 http://localhost:8081/vote/weekly?page=1&sex=0 (특정 성별만 가져옴)
 
 3) 월간 최대 투표 사진 가져오기
 HTTP GET
 http://localhost:8081/vote/monthly?page=1
 http://localhost:8081/vote/monthly?page=1&sex=1 (특정 성별만 가져옴)

 4) 전체 순위
 HTTP GET:
 http://localhost:8081/vote/total?page=1
 {'1': [{'votes': [5L, 1L, 0L, 0L, 0L, 0L], 'url': u'http://a4.sphotos.ak.fbcdn.net/hphotos-ak-ash4/s480x480/313242_114370755376042_768572609_n.jpg', 'skin': 0L, 'datetime': '2012-07-29 12:37:09.636346', 'url2': u'http://a4.sphotos.ak.fbcdn.net/hphotos-ak-ash4/s320x320/313242_114370755376042_768572609_n.jpg', 'sex': 0L, 'id': '5'}, ...
 
 http://localhost:8081/vote/total?page=1&sex=1

 :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 1) 특정 identity가 특정 사진에 대해 이미 vote한적이 있는지 조회

 HTTP GET:
 http://<server>/vote?photoid=3&identity=wonkim
 RETURN:
 'yes' or 'no'
 투표한적이 있으면 yes 문자열. 아니면 no 문자열

 2) Voting API
 HTTP PUT:
 http://<server>/vote?photoid=1&votetype=1&identity=wonkim
 위와 같이 identity 인자가 추가 됨. 누가 voting하는 것인지 확인하기 위함.
 photoid: ID of uploaded photo
 votetype: type of voting from 0 ~ 7 (please use it as client coding)

 RETURN:
 Success: Total Vote Count & 200 OK
 Fail: 404 Not Found (Id is wrong or VoteType is wrong)

 Photo 정보를 조회하는 API :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 HTTP GET
 http://localhost:8081/photo?photoid=1&urlonly=1
 
 photoid: id of photo
 urlonly: if you want to get url information only set it as 1. if not do not add this argument
 
 Return:
 {"photos": {"1": {"url": "www.facebook.com/test/hello.jpg", "skin": 0, "votes": [0, 0, 0, 0, 0, 0, 0, 0], "sex": 1, "datetime": "2012-07-02 14:43:19.351239"}}}
 
 URL only:
 {"photos": {"1": {"url": "www.facebook.com/test/hello.jpg"}}}

 :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 사진 데이터 저장
 HTTP PUT 
 http://facevotapi.appspot.com/photo?identity=wonkim&url=www.facebook.com&url2=www.face.com&sex=0&skin=1

 */

#import <Foundation/Foundation.h>

typedef void (^completeHandler)(NSArray *picList);

@interface NetInterface : NSObject

+ (void) publicTimelineGender:(BOOL)gender page:(NSUInteger) page
                   onComplete:(completeHandler) handler
                      onError:( void(^)(NSError*)) errHandler;

// 내가 올린 사진들의 목록을 가져온다. 페이지 단위로 가져오도록 되어 있음.
+ (void) myTimelinePage:(NSUInteger) page
               identity:(NSString*) identity
             OnComplete:(void(^)(NSDictionary*)) handler
                onError:( void(^)(NSError*)) errHandler;

+ (void) weeklyWinnerGender:(BOOL)gender page:(NSUInteger) page
                 onComplete:( void(^)(NSDictionary*)) handler
                    onError:( void(^)(void)) errHandler;

+ (void) monthlyWinnerGender:(BOOL)gender page:(NSUInteger) page
                 onComplete:( void(^)(NSDictionary*)) handler
                    onError:( void(^)(void)) errHandler;

+ (void) totalWinnerGender:(BOOL)gender page:(NSUInteger) page
                  onComplete:( void(^)(NSDictionary*)) handler
                     onError:( void(^)(void)) errHandler;

//즐겨 찾기 목록을 서버에서 가져온다.
+ (void) favoTimelinePage:(NSUInteger)page
               onComplete:(completeHandler) handler
                  onError:( void(^)(void)) errHandler;

// 즐겨 찾기 항목을 등록하거나 삭제한다.
+ (void) setFavoriteManId:(NSString*) targetId
             withPhottoId:(NSString*) photoId
                boolValue:(BOOL)setVal
                     name:(NSString*) herName
               onComplete:( void(^)(BOOL)) handler
                  onError:( void(^)(void)) errHandler;

// 페이스북 앨범에 업로드 된 사진의 URL 을 가지고, 우리 서비스에 새 사진을 등록한다.
+ (void) postMyPhotoURL:(NSString*)urlString
               thumbURL:(NSString*)thumbUrlString
             onComplete:( void(^)(NSString*)) handler
                onError:( void(^)(NSError*)) errHandler;

+ (void) didVoteAtPhotoID:(NSString*)photoId
                  onComplete:( void(^)(BOOL)) handler
                     onError:( void(^)(void)) errHandler;

+ (void) doVoteAtPhotoID:(NSString*)photoId vote:(NSUInteger)vote
              onComplete:( void(^)(void)) handler
                 onError:( void(^)(void)) errHandler;

+ (BOOL) facebookLogin:( void(^)(BOOL)) handler showUI:(BOOL)showIt;

@end
