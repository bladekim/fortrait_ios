//
//  UserContext.h
//  PuddingSNS
//
//  Created by bladekim on 10. 4. 13..
//  Copyright 2010 ChocolateSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import "WaitView.h"

#define USERCONTEXT     [UserContext sharedUserContext]
#define ITEM_COR        USERCONTEXT.itemColor
#define ITEM_TXTW       USERCONTEXT.itemTextGirl
#define ITEM_TXTM       USERCONTEXT.itemTextMan
#define ITEM_TXT(_g_)   ((_g_)?ITEM_TXTW:ITEM_TXTM)

@interface UserContext: NSObject 
{
	WaitView	*waitV;
}

@property (nonatomic, strong)   NSString    *appName;
@property (nonatomic, strong)   NSString    *myName;
@property (nonatomic)           BOOL        myGender;
@property (nonatomic, strong)   NSString    *myIdentity;
@property (nonatomic, strong)   NSString    *myLocale;

@property (nonatomic, strong)   NSArray     *itemColor;
@property (nonatomic, strong)   NSArray     *itemTextGirl;
@property (nonatomic, strong)   NSArray     *itemTextMan;

@property (assign) BOOL needReloadFavo;

@property (assign) BOOL showPopover;

+ (UserContext *)sharedUserContext;

- (id) initWithDefault;

- (void) startWaitView: (NSInteger) yDeltaPos;
- (void) stopWaitView;
- (void) completeView;
- (void) splashEffectPoint:(CGPoint)point atView:(UIView*) view;

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;

@end
