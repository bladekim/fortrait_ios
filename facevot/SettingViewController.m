//
//  SettingViewController.m
//  facevot
//
//  Created by 태한 김 on 12. 7. 4..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//

#import "SettingViewController.h"
#import "LegalViewController.h"

enum E_SECTION_NAME {
    ABOUT_AC = 0,
    LEGAL_AC = 1,
    MAX_AC = 2
};

@implementation SettingViewController

-(id) init
{
    self = [super init];
    if( self ) {
        self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        [self.tableView registerClass:[UITableViewCell class]
               forCellReuseIdentifier:@"setCell_default"];

        self.tableView.delegate = self;
        self.tableView.dataSource = self;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - UITableView dataSource, delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return MAX_AC;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case ABOUT_AC:
            return 2;
        case LEGAL_AC:
            return 1;
    }
    return 1;  // nothing.
}

- (CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45.0;
}

- (UITableViewCell *)tableView:(UITableView *)tView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tView dequeueReusableCellWithIdentifier:@"setCell_default" forIndexPath:indexPath];

    switch (indexPath.section) {
        case ABOUT_AC:
            switch (indexPath.row) {
                case 0:
                    [cell.textLabel setText:@"About Facevot"];
                    break;
                case 1:
                    [cell.textLabel setText:@"Contact Us"];
                    break;
            }
            break;

        case LEGAL_AC:
            [cell.textLabel setText:@"Terms of Use"];
            break;
    }

    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case ABOUT_AC:
            switch (indexPath.row) {
                case 0:
                    break;
                case 1:
                    break;
            }
            break;

        case LEGAL_AC:
        {
            LegalViewController *lvc = [[LegalViewController alloc] init];
            [self.navigationController pushViewController:lvc
                                                 animated:YES];
        }
            break;
    }
}

@end
