//
//  AppDelegate.m
//  SwipeMenuDemo
//
//  Created by YoonBong Kim on 12. 3. 19..
//  Copyright (c) 2012년 KTH. All rights reserved.
//

#import "AppDelegate.h"
#import "YBSwipeViewController.h"
#import "Parse/Parse.h"

@implementation AppDelegate

@synthesize window = _window;

NSString *const FBSessionStateChangedNotification = @"com.chcolate.fortrait:FBSessionStateChangedNotification";


- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];

    /* initializing */
    stageViewCntrlr = [[StageViewController alloc] init];
    UINavigationController *mainNavigationCntrlr = [[UINavigationController alloc] initWithRootViewController:stageViewCntrlr];

    menuViewCntrlr = [[MenuViewController alloc] init];
    UINavigationController *menuNavigationCntrlr = [[UINavigationController alloc] initWithRootViewController:menuViewCntrlr];


    /* Swipe View Controller is needed at least two view controllers that menu and main */
    YBSwipeViewController *swipeRootViewCntrlr = [[YBSwipeViewController alloc] initWithMainViewController:mainNavigationCntrlr menuViewController:menuNavigationCntrlr];

    self.window.rootViewController = swipeRootViewCntrlr;

    [self.window makeKeyAndVisible];

    // 초기 값 설정. 스위치 값들은 기본적으로 woman 으로 설정한다.
    if( ![[NSUserDefaults standardUserDefaults] boolForKey:@"isFirst"] )
    {
        [[NSUserDefaults standardUserDefaults] setBool:GIRL forKey:@"StageSwitch"];
        [[NSUserDefaults standardUserDefaults] setBool:GIRL forKey:@"WinnerSwitch"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirst"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }

    return YES;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [Parse setApplicationId:@"YbBKs5HMpLqgnmesVB96lZmtykLuSUFjv3iYhH5D"
                  clientKey:@"gKZ5lpL1CwaoGL42ytZuePI5S7G0vSlCxSHBYylY"];

    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    // attempt to extract a token from the url
    return [FBSession.activeSession handleOpenURL:url];
}

-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [PFPush storeDeviceToken:deviceToken];
    [PFPush subscribeToChannelInBackground:@"" block:^(BOOL succeeded, NSError *error) {
        if (succeeded)
            NSLog(@"Successfully subscribed to broadcast channel!");
        else
            NSLog(@"Failed to subscribe to broadcast channel; Error: %@",error);
    }];
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Failed to register for push, %@",error);
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // FBSample logic
    // if the app is going away, we close the session object
    [FBSession.activeSession close];

    // pList logic
}

#pragma mark - Facebook related methods

- (void)sessionStateChanged:(FBSession *)session
                      state:(FBSessionState)state
                    handler:( void(^)(BOOL)) handler
                      error:(NSError *)error
{
    switch (state) {
        case FBSessionStateOpen:
            if( error ) break;
            // We have a valid session
            NSLog(@"User session found");
        {
            // 로그인 되면, 기본적인 페이스북의 개인 정보를 읽어와야 한다.
                
            FBRequest *req = [FBRequest requestForGraphPath:@"me"];
            [req startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                USERCONTEXT.myName = [result objectForKey:@"name"];
                USERCONTEXT.myIdentity = [result objectForKey:@"id"];
                USERCONTEXT.myGender = [[result objectForKey:@"gender"] isEqualToString:@"female"];
                USERCONTEXT.myLocale = [result objectForKey:@"locale"];
                NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                [ud setBool:USERCONTEXT.myGender forKey:@"myGender"];
                [ud setObject:USERCONTEXT.myName forKey:@"myName"];
                [ud setObject:USERCONTEXT.myIdentity forKey:@"myIdentity"];
                [ud setObject:USERCONTEXT.myLocale forKey:@"myLocale"];
                [ud synchronize];
                
                dispatch_async(dispatch_get_main_queue(), ^(){
                    STOP_WAIT_VIEW;
                });
                handler(YES);
            }];
            break;
        }
        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed:
            [FBSession.activeSession closeAndClearTokenInformation];
            handler(NO);
            break;
        default:
            break;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:FBSessionStateChangedNotification
                                                        object:session];

    if (error) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:error.localizedDescription
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
}

- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI
                            handler:( void(^)(BOOL)) handler
{
    NSArray *permissions = [NSArray arrayWithObjects: @"user_photos", @"user_about_me", nil];
    return [FBSession openActiveSessionWithReadPermissions:permissions
                                              allowLoginUI:allowLoginUI
                                         completionHandler:^(FBSession *session,FBSessionState state,NSError *error)
    {
        [self sessionStateChanged:session state:state
                          handler:handler error:error];
    }];
}

@end
