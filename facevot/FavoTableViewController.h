//
//  FavoTableViewController.h
//  facevot
//
//  Created by 김태한 on 12. 6. 30..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//

#import "BaseViewController.h"
#import "LKQueue.h"

@interface FavoTableViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate>
{
    NSMutableArray *fList;
    LKQueue *fQueue;
    BOOL endOfList;
    NSUInteger page;
}

@end
