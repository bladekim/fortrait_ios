//
//  MainViewController.m
//  FaceVot Demo
//
//  Created by Blade Kim on 12. 6. 5..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//

#import <MobileCoreServices/MobileCoreServices.h>
#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import "MainViewController.h"
#import "YBSwipeViewController.h"
#import "TSActionSheet.h"
#import "AppDelegate.h"
#import "NewPicViewController.h"
#import "MyDetailViewController.h"
#import "NetInterface.h"
#import "LKQueueManager.h"
#import "CSFBAlbumTableController.h"

#define MYQUEUE_ID    @"myListQueue"

@implementation MainViewController

- (id) initWithIdentity:(NSString*)identity name:(NSString*)theName
{
    self = [super init];
    if (self) {
        self.title = theName;
        theIdentity = identity;
        self.tableView.delegate = self;
        self.tableView.dataSource = self;

        [self.tableView registerClass:[MyCell class] forCellReuseIdentifier:@"myCell"];
        [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];

        UIRefreshControl *refreshCtrl = [[UIRefreshControl alloc] init];
        [refreshCtrl setTintColor:[UIColor darkGrayColor]];
        [refreshCtrl addTarget:self action:@selector(refreshMe:) forControlEvents:UIControlEventValueChanged];
        [self setRefreshControl:refreshCtrl];

        myList = [[NSMutableArray alloc] initWithCapacity:20];
        tmpScore = 0;

        // '나'인 경우만 queue 를 사용하자.
        if( [theName isEqualToString:@"My Fortrait"])
        {
            listQueue = [[LKQueueManager defaultManager] queueWithName:MYQUEUE_ID];
            // 그리고 저장된 특표 현황.
            tmpScore = [[NSUserDefaults standardUserDefaults] integerForKey:@"myScore"];
            myItemNumsArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"votelistArray"];
        }
    }
    return self;
}


- (void)loadView
{
    [super loadView];

    USERCONTEXT;

    self.view.backgroundColor = WHITE_C;

    UIBarButtonItem *picButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"camBtn.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(camBtnAction:)];
    self.navigationItem.rightBarButtonItem = picButtonItem;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    if( 0 == [myList count] ) {
        page = 1;

        if( [self.title isEqualToString:@"My Fortrait"])
        {
            LKQueueEntry *entry = [listQueue entryAtIndex:0];
            [myList addObjectsFromArray:(NSArray*)(entry.resources)];

            theGender = USERCONTEXT.myGender;
        }
        else // 진짜 내가 아니고 다른 특정한 사람이라면 무조건 네트워크 로딩.
        {
            [self refreshMe:self.refreshControl];
            // 그리고 사진 올리기 버튼이 필요 없다.
            UIBarButtonItem *fbButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"facebookIcon.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(fbBtnAction:)];
            self.navigationItem.rightBarButtonItem = fbButtonItem;
        }
        if( 20 > [myList count] )
            endOfList = YES;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}


-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
///    [numView add:180];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark - UITableView dataSource, delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if( 1 == section ) return 0;

    return( [myList count] + 1 );
}

- (CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( 0 == indexPath.row ) return MY_INFO_H;

    return MY_CELL_H;
}

- (UITableViewCell *)tableView:(UITableView *)tView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( 0 == indexPath.row )  // 여기는 사용자 득표 숫자를 보여주기 위한 헤더 셀.
    {
        UITableViewCell *hCell = [tView dequeueReusableCellWithIdentifier:@"myCountCell"];
        if( nil == hCell )
        {
            hCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"myCountCell"];
            [hCell.contentView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"myinfo_back.png"]]];
            [hCell setClipsToBounds:YES];
//            UILabel *myPics = [[UILabel alloc] initWithFrame:CGRectMake(10, 3, 60, 12)];
//            [myPics setBackgroundColor:[UIColor clearColor]];
//            [myPics setFont:CS_FONT(10)];
//            [myPics setTextAlignment:NSTextAlignmentLeft];
//            [myPics setTextColor:[UIColor whiteColor]];
//            [myPics setShadowColor:[UIColor blackColor]];
//            [myPics setShadowOffset:CGSizeMake(0, 1)];
//            [myPics setText:@"Count"];

            numView = [[UILabel alloc] initWithFrame:CGRectMake(180, 0, 110, MY_INFO_H-1)];
            [numView setBackgroundColor:[UIColor whiteColor]];
            [numView setFont:CS_FONT(18)];
            [numView setTextColor:[UIColor blackColor]];
            [numView setTextAlignment:NSTextAlignmentRight];
            [hCell.contentView addSubview:numView];

            mostText = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 150, MY_INFO_H-1)];
            [mostText setBackgroundColor:[UIColor whiteColor]];
            [mostText setFont:[UIFont fontWithName:@"TimesNewRomanPSMT" size:17.0]];
            [mostText setTextColor:[UIColor darkGrayColor]];
            [hCell.contentView addSubview:mostText];

            UILabel *vote = [[UILabel alloc] initWithFrame:CGRectMake(292, 22, 30, 9)];
            [vote setBackgroundColor:[UIColor whiteColor]];
            [vote setFont:CS_FONT(9)];
            [vote setTextColor:[UIColor grayColor]];
            [vote setText:@"vote"];
            [hCell.contentView addSubview:vote];

            [hCell.textLabel setText:@"Logging in with Facebook"];
            [hCell.textLabel setTextAlignment:NSTextAlignmentCenter];
            [hCell.textLabel setBackgroundColor:[UIColor clearColor]];
            [hCell.textLabel setFont:CS_FONT(18)];
        }

        // 로그인 세션이 열린 상태면 개인 정보를 보여준다.
        if( [FBSession.activeSession isOpen] )
        {
            [numView setHidden:NO];
            [mostText setHidden:NO];
            [hCell.textLabel setText:@""];
            [self performSelector:@selector(updateCounter) withObject:nil afterDelay:0.2];
        }
        else // 로그인 되지 않은 상태라면 로그인을 진행하고, 로그인 완료 후 테이블을 다시 갱신할 것이다.
        {
            [numView setHidden:YES];
            [mostText setHidden:YES];
//            if( ![NetInterface facebookLogin:^(BOOL result){
//                if( result ){
//                    if( [self.title isEqualToString:@"Me"])
//                        [self.tableView reloadData];  // 나 라면 그냥 다시 테이블 갱신.
//                    else
//                        [self refreshMe:self.refreshControl]; // 남들은 서버로부터 읽기.
//                }
//            } showUI:NO] )
            {
                [NetInterface facebookLogin:^(BOOL result){
                    if( result ){
                        if( [self.title isEqualToString:@"My Fortrait"])
                            [self.tableView reloadData];  // 나 라면 그냥 다시 테이블 갱신.
                        else
                            [self refreshMe:self.refreshControl]; // 남들은 서버로부터 읽기.
                    }
                } showUI:YES];
            }
        }
        return hCell;
    }

    // 보통 셀.
    MyCell *cell = [tView dequeueReusableCellWithIdentifier:@"myCell" forIndexPath:indexPath];

    NSDictionary *cellDic = myList[indexPath.row - 1];

    [cell setGender:theGender];
    [cell setImageURL:cellDic[@"url2"] numbers:cellDic[@"votes"] index:indexPath.row - 1];
    cell.delegate = self;

    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    //
    if( 0 == indexPath.row ) return;

    [self enableMenuGesture:NO];

    MyDetailViewController *mV = [[MyDetailViewController alloc] initWithDic:myList[indexPath.row - 1]];
    [self presentSemiViewController:mV];
}

// 셀에서 사진만 선택한 경우. 사진 확대 보기 효과를 주면서 상세 보기 화면으로 전환한다.
- (void) selectedMyCell:(NSUInteger)idx thumbView:(UIImageView *)view
{
/*
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    tRect = [view convertRect:view.frame toView:appDelegate.window];
    if( nil == tmpImgV ){
        tmpBackImgV = [[UIImageView alloc] initWithFrame:appDelegate.window.frame];
        [tmpBackImgV setImage:[UIImage imageNamed:@"detailBackImg.png"]];
        [tmpBackImgV setAlpha:0.0];
        [appDelegate.window addSubview:tmpBackImgV];

        tmpImgV = [[UIImageView alloc] initWithImage:view.image];
        [tmpImgV setContentMode:UIViewContentModeScaleAspectFill];
        [tmpImgV setFrame:tRect];
        [appDelegate.window addSubview:tmpImgV];
    }

    [UIView animateWithDuration:0.4 animations:^(void) {
        [tmpImgV setFrame:CGRectMake(0, ([UIScreen mainScreen].bounds.size.height+20-MY_CELL_H)/2-44, 320, MY_CELL_H*2)];
        [tmpBackImgV setAlpha:1.0];
    } completion:^(BOOL finished)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            DetailViewController *detailVC = [[DetailViewController alloc] initWithDic:myList[idx]];
            [detailVC setTempImage:tmpImgV.image];
            [detailVC setCaller:self];
            
            [self.navigationController pushViewController:detailVC animated:NO];
            [tmpImgV setAlpha:0.0];
            [tmpBackImgV setAlpha:0.0];
        });
    }];
*/
    DetailViewController *detailVC = [[DetailViewController alloc] initWithDic:myList[idx]];
    [detailVC setTempImage:tmpImgV.image];
    [detailVC setCaller:self];

    [self.navigationController pushViewController:detailVC animated:YES];
}

- (void) returnAnimation
{
//    [tmpImgV setAlpha:1.0];
//    [tmpBackImgV setAlpha:1.0];

    [[self navigationController] setNavigationBarHidden:NO animated:NO];

//    [UIView animateWithDuration:0.4 animations:^(void) {
//        [tmpImgV setFrame:tRect];
//        [tmpBackImgV setAlpha:0.0];
//    } completion:^(BOOL finished)
//    {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [UIView animateWithDuration:0.2 animations:^(void) {
//                [tmpImgV setAlpha:0.0];
//            } completion:^(BOOL finished) {
//                [tmpImgV removeFromSuperview];
//                [tmpBackImgV removeFromSuperview];
//                tmpImgV = nil;
//                tmpBackImgV = nil;
//            }];
//        });
//    }];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if( 1 == section ) {
        footer = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, MY_INFO_H)];
        if ( !endOfList )
        {
            [footer setTitle:@"more" forState:UIControlStateNormal];
            [footer.titleLabel setFont:CS_BOLD_FONT(12.0)];
            [footer addTarget:self action:@selector(readMore) forControlEvents:UIControlEventTouchUpInside];
            [footer setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"myinfoBack.png"]]];
        } else {
            [footer setTitle:@"" forState:UIControlStateNormal];
            [footer setUserInteractionEnabled:NO];
        }
    
        return footer;
    }

    return nil;
}

- (void) updateCounter
{
    // number with comma
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numView setText: [formatter stringFromNumber:@(tmpScore)]];

    // 여기서 숫자 옆에 텍스트 표시도 함께 해 주자.
    for( NSUInteger i = 0, ix = 0 ; i < myItemNumsArray.count; i++ )
    {
        if( ix <= [myItemNumsArray[i] integerValue] ){
            ix = [myItemNumsArray[i] integerValue];
            [mostText setText:[NSString stringWithFormat:@"I'm %@",ITEM_TXT(theGender)[i]]];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if( 1 == section ) return 43; // more button height.

    return 0;
}

#pragma mark - refresh

- (void) refreshMe:(id) refreshCtrl
{
    NSLog(@"refresh notification.");

    if( [FBSession.activeSession isOpen] )
    {
        [NetInterface myTimelinePage:1
                            identity:theIdentity
                          OnComplete:^(NSDictionary *myDic)
        {
            [myList removeAllObjects];
            [myList addObjectsFromArray:myDic[@"photos"]];
            [refreshCtrl endRefreshing];
            page = 1;
            tmpScore = [myDic[@"votes"] integerValue];
            myItemNumsArray = myDic[@"voteslist"];

            theGender = (BOOL)(myDic[@"photos"][0][@"sex"]);
            if( [self.title isEqualToString:@"My Fortrait"]) {
                [listQueue removeAllEntries];
                [listQueue addEntryWithInfo:@{@"title":@"allMyList"}
                                  resources:myList
                                    tagName:nil];

                [[NSUserDefaults standardUserDefaults] setInteger:tmpScore forKey:@"myScore"];
                [[NSUserDefaults standardUserDefaults] setObject:myItemNumsArray forKey:@"votelistArray"];
                
            }
            if( 20 > [myDic[@"photos"] count] )
                endOfList = YES;

            dispatch_async(dispatch_get_main_queue(), ^(){
                [self.tableView reloadData];
                [self updateCounter];
            });
        }
                             onError:^(NSError *error)
        {
            [refreshCtrl endRefreshing];
            NOTI(@"Network Error");
            NSLog(@"error:%@",error.description);
        }];
    }
}

// 카메라 버튼을 누르면, 사진 앨범에서 선택
- (void) camBtnAction: (UIButton*)sender
{
    TSActionSheet *camActSheet = [[TSActionSheet alloc] initWithTitle:@"Get photo from..."];
    [camActSheet setBackgroundColor:CS_RGB(20, 20, 20)];

    [camActSheet addButtonWithTitle:@"Library" block:^(void) {
        if( nil == imgPicker )
            imgPicker = [[UIImagePickerController alloc] init];

        [imgPicker setDelegate:self];
        [imgPicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        [self presentViewController:imgPicker animated:YES completion:^(void){}];
    }];

    if( [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront] )
    {
        [camActSheet addButtonWithTitle:@"Camera" block:^(void) {
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
            {
                if( nil == imgPicker )
                    imgPicker = [[UIImagePickerController alloc] init];
                [imgPicker setDelegate:self];
                imgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                imgPicker.mediaTypes = @[(NSString *)kUTTypeImage];
                imgPicker.allowsEditing = NO;
                dispatch_async(dispatch_get_main_queue(), ^(){
                    [self presentViewController:imgPicker animated:YES completion:^(void){}];
                });
            }
        }];
    }
    if( [FBSession.activeSession isOpen] )
    {
        [camActSheet addButtonWithTitle:@"Facebook Album" block:^(void)
        {
            [self enableMenuGesture:NO];

            CSFBAlbumTableController *fbAlbumVC = [[CSFBAlbumTableController alloc] init];
            UINavigationController *faNavi = [[UINavigationController alloc] initWithRootViewController:fbAlbumVC];
            [self presentViewController:faNavi animated:YES completion:^{
            }];
        }];
    }

//    [camActSheet showWithRect:CGRectOffset(sender.frame, 0.0, 46.0)];
    [camActSheet showWithRect:CGRectMake(280, 10, 40, 44)];
}

- (void) fbBtnAction: (id)sender
{
//    [FBRequest startWithGraphPath:theIdentity
//                completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
//    {
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[result objectForKey:@"link"]]];
//    }];
    FBRequest *req = [FBRequest requestForGraphPath:theIdentity];
    [req startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[result objectForKey:@"link"]]];
    }];

}

- (void) readMore
{
    NSLog(@"read more.");
    page++;

    [NetInterface myTimelinePage:page
                        identity:theIdentity
                      OnComplete:^(NSDictionary *myDic)
     {
         [myList addObjectsFromArray:myDic[@"photos"]];
         
         if( 20 > [myDic[@"photos"] count] )
             endOfList = YES;

         dispatch_async(dispatch_get_main_queue(), ^(){
             [self.tableView reloadData];
         });
     }
                         onError:^(NSError *error)
     {
         NOTI(@"Network Error");
         NSLog(@"error:%@",error.description);
     }];
}

#pragma mark - Image Picker Delegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:^(void){}];
}

// 이미지가 선택되면 여기에서 포스팅 작업을 하자.
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)Info
{
    [picker dismissViewControllerAnimated:YES completion:^(void)
    {
        STOP_WAIT_VIEW;

        NewPicViewController *newPicVC = [[NewPicViewController alloc] init];
        [newPicVC setImage:image];
        [self.navigationController pushViewController:newPicVC animated:YES ];
    }];
}

#pragma mark -

- (void) insertPhotoUrl:(NSString*)address thumbUrl:(NSString*) thumbAddr
{
    NSDictionary *picDic = @{ @"votes": @[@(0),@(0),@(0),@(0),@(0),@(0)],
        @"url": address, @"sex":@(USERCONTEXT.myGender),
        @"url2": thumbAddr,
        @"id": @"0" };
    [myList insertObject:picDic atIndex:0];

    [self.tableView reloadData];
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

// 사진 등록하다가 오류가 발생한 경우 다시 취소하기 위한 용도로 사용됨.
- (void) removeLatestItem
{
    [myList removeObjectAtIndex:0];
    [self.tableView reloadData];
} 

// 지금 막 등록된 아이템의 아이디 값을 갱신한다.
- (void) uplodedPhotoID:(NSString*) new_id
{
    NSMutableDictionary *bornItem = [[myList objectAtIndex:0] mutableCopy];

    [bornItem setObject:new_id forKey:@"id"];

    [myList setObject:bornItem atIndexedSubscript:0];

    // backup to queue.
    if( [self.title isEqualToString:@"My Fortrait"]) {
        [listQueue removeAllEntries];
        [listQueue addEntryWithInfo:@{@"title":@"allMyList"}
                          resources:myList
                            tagName:nil];

    }
    NSLog(@"photo update complete");
}

@end
