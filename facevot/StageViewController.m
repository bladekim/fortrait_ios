//
//  StageViewController.m
//  facevot
//
//  Created by 태한 김 on 12. 9. 25..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//

#import <MobileCoreServices/MobileCoreServices.h>
#import "AppDelegate.h"
#import "StageViewController.h"
#import "StageCell.h"
#import "TSActionSheet.h"
#import "NewPicViewController.h"
#import "PhotoViewController.h"
#import "NetInterface.h"
#import "LKQueueManager.h"
#import "CSFBAlbumTableController.h"

#define PBQUEUE_ID    @"publicListQueue"
#define ADD_BTN_Y      49

@implementation StageViewController

- (id)init
{
    UICollectionViewFlowLayout *mylayout = [[UICollectionViewFlowLayout alloc] init];
    [mylayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    self = [super initWithCollectionViewLayout:mylayout];

    if (self) {
        // Custom initialization
        self.title = @"Fortrait";
        [self.collectionView registerClass:[StageCell class] forCellWithReuseIdentifier:@"SCell"];

        [self.collectionView setDelegate:self];
        [self.collectionView setDataSource:self];

        [self.collectionView setBackgroundColor:WHITE_C];

        // 사진 첨부 버튼.
        addBtn = [[UIButton alloc] initWithFrame:CGRectMake(160-20, ADD_BTN_Y, 40, 22)];
        [addBtn setBackgroundImage:[UIImage imageNamed:@"addBtn.png"] forState:UIControlStateNormal];
        [addBtn addTarget:self action:@selector(photoAction:) forControlEvents:UIControlEventTouchUpInside];
        [((AppDelegate*)([UIApplication sharedApplication].delegate)).window addSubview:addBtn];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    UIRefreshControl *refreshCtrl = [[UIRefreshControl alloc] init];
    [refreshCtrl setTintColor:[UIColor darkGrayColor]];
    [refreshCtrl addTarget:self action:@selector(refreshStream:) forControlEvents:UIControlEventValueChanged];
    [refreshCtrl.viewForBaselineLayout setFrame:CGRectMake(0, -50, 320, 50)];
    [self.collectionView addSubview:refreshCtrl.viewForBaselineLayout];

    // 우측 상단에는 남/여를 선택할 수 있는 스위치가 있다.
    genderBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 27)];
    if( [[NSUserDefaults standardUserDefaults] boolForKey:@"StageSwitch"] )
        [genderBtn setBackgroundImage:[UIImage imageNamed:@"g_woman.png"] forState:UIControlStateNormal];
    else
        [genderBtn setBackgroundImage:[UIImage imageNamed:@"g_man.png"] forState:UIControlStateNormal];

    [genderBtn addTarget:self action:@selector(toggleGender:) forControlEvents:UIControlEventTouchUpInside];

    UIBarButtonItem *bItem = [[UIBarButtonItem alloc] initWithCustomView:genderBtn];
    self.navigationItem.rightBarButtonItem = bItem;

    pbList = [[NSMutableArray alloc] initWithCapacity:40];
    listQueue = [[LKQueueManager defaultManager] queueWithName:PBQUEUE_ID];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    if( 0 == [pbList count] ) {
        LKQueueEntry *entry = [listQueue entryAtIndex:0];
        [pbList addObjectsFromArray:(NSArray*)(entry.resources)];
        
        if( 0 == pbList.count ){
            START_WAIT_VIEW;
            [self refreshStream:nil];  // 캐쉬에 아무것도 없어? 그럼 그냥 다시 서버에서 읽자.
        }
        _reading = NO;
    }
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    if( nil == addBtn ){
        // 사진 첨부 버튼.
        addBtn = [[UIButton alloc] initWithFrame:CGRectMake(160-20, ADD_BTN_Y, 40, 22)];
        [addBtn setBackgroundImage:[UIImage imageNamed:@"addBtn.png"] forState:UIControlStateNormal];
        [addBtn addTarget:self action:@selector(photoAction:) forControlEvents:UIControlEventTouchUpInside];
        [((AppDelegate*)([UIApplication sharedApplication].delegate)).window addSubview:addBtn];
    }
    [((AppDelegate*)([UIApplication sharedApplication].delegate)).window bringSubviewToFront:addBtn];
}

- (void) hideAddButton:(BOOL) isHide
{
    if( addBtn ){
        [UIView animateWithDuration:0.4 animations:^{
            if( isHide )
                [addBtn setAlpha:0.0];
            else
                [addBtn setAlpha:1.0];
        }];
    }
}

#pragma mark - CollectionView layout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat cellWidth;
    NSUInteger idx = indexPath.row % 4;

    if( 0 == idx || 3 == idx ) cellWidth = 180;
    else if( 1 == idx || 2 == idx ) cellWidth = 115;

    return CGSizeMake(cellWidth, 135);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 7.0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(9, 9, 0, 9);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 6.0;
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [pbList count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    StageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SCell" forIndexPath:indexPath];
    NSDictionary *cellDic = pbList[indexPath.row];

    if( ![cellDic[@"url2"] isKindOfClass:[NSNull class]] )
        [cell setImageURL:cellDic[@"url2"]];
    else
        [cell setImageURL:cellDic[@"url"]];
    
    [cell setFavoriteMark: [cellDic[@"favorite"] isEqualToNumber:@(1)] ];   // 좋아요 등록된 사람이라면 리본 마크를 붙여주세요.

    return cell;
}

- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"selected cell %d", indexPath.row);
    PhotoViewController *pvc = [[PhotoViewController alloc] init];
    pvc.listDics = pbList;
    [self.navigationController pushViewController:pvc animated:YES];
    [pvc setStartIndex:indexPath.row];
}

#pragma mark - refresh and read more

- (void) refreshStream:(id) refreshCtrl
{
    NSLog(@"refresh notification.");
    _reading = YES;
    
    [NetInterface publicTimelineGender:[[NSUserDefaults standardUserDefaults] boolForKey:@"StageSwitch"]
                                  page:1
                            onComplete:^(NSArray *picList)
     {
         endOfList = NO;
         if( 20 > picList.count ) endOfList = YES;
         
         [pbList removeAllObjects];
         [pbList addObjectsFromArray:picList];
         
         [listQueue removeAllEntries];
         [listQueue addEntryWithInfo:@{@"title":@"publicList"}
                           resources:pbList
                             tagName:nil];
         
         if( nil != refreshCtrl )
             [refreshCtrl endRefreshing];
         
         dispatch_async(dispatch_get_main_queue(), ^(){
             _reading = NO;
             [self.collectionView reloadData];
             STOP_WAIT_VIEW;
         });
     }
                               onError:^(NSError *err)
     {
         _reading = NO;
         if( nil != refreshCtrl )
             [refreshCtrl endRefreshing];
         NOTI(@"Network Error");
         STOP_WAIT_VIEW;
         NSLog(@"error:%@",err.description);
     }];
}

- (void) toggleGender:(UIButton*)sender
{
    // 스위치 설정은 토글되어 저장되어야 한다.
    if( [[NSUserDefaults standardUserDefaults] boolForKey:@"StageSwitch"] )
    {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"StageSwitch"];
        [genderBtn setBackgroundImage:[UIImage imageNamed:@"g_man.png"] forState:UIControlStateNormal];
    } else {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"StageSwitch"];
        [genderBtn setBackgroundImage:[UIImage imageNamed:@"g_woman.png"] forState:UIControlStateNormal];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];

    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:genderBtn cache:YES];
    [UIView commitAnimations];

//    [UIView animateWithDuration:0.5
//                     animations:^(void){
//                         [self.collectionView setAlpha:0.0];
//                     }
//                     completion:^(BOOL finish){
//                         if( finish ){
//                             dispatch_async(dispatch_get_main_queue(), ^(){
//                                 [self.collectionView setAlpha:1.0];
//                             });
//                         }
//                     }];

    START_WAIT_VIEW;
    [self refreshStream:nil];
}

- (void) readMore
{
    NSLog(@"read more.");
    if( _reading ) return;
    
    _reading = YES;
    page ++;

    [NetInterface publicTimelineGender:[[NSUserDefaults standardUserDefaults] boolForKey:@"StageSwitch"]
                                  page:page
                            onComplete:^(NSArray *picList)
     {
         [pbList addObjectsFromArray:picList];
         
         //         [listQueue removeAllEntries];
         //         [listQueue addEntryWithInfo:@{@"title":@"publicList"}
         //                           resources:pbList
         //                             tagName:nil];
         if( 20 > [picList count] )
             endOfList = YES;
         
         dispatch_async(dispatch_get_main_queue(), ^(){
             [self.collectionView reloadData];
             _reading = NO;
         });
     }
                               onError:^(NSError *err)
     {
         _reading = NO;
         NOTI(@"Network Error");
         NSLog(@"error:%@",err.description);
     }];
}

#pragma mark -

- (void) photoAction:(UIButton*)sender
{
    // 페이스북 로그인이 되어 있지 않다면 로그인 하고나서 다시 재귀호출한다.
    if( ![FBSession.activeSession isOpen] )
    {
        if( ![FBSession.activeSession isOpen] )
        {
//            if( ![NetInterface facebookLogin:^(BOOL result)
//                  {
//                      if( result ){
//                          [self photoAction:sender];
//                      }
//                  } showUI:NO] )
            {
                [NetInterface facebookLogin:^(BOOL result)
                 {
                     if( result ){
                         [self photoAction:sender];
                     }
                 } showUI:YES];
            }
        }
        return;
    }


    [self hideAddButton:YES];

    if( nil == btnBar )
    {
        btnBar = [[UIView alloc] initWithFrame:CGRectMake(0, 44, 320, 45)];
//        [btnBar setAutoresizesSubviews:YES];
        UIButton *libBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 106, 45)];
        [libBtn setBackgroundImage:[UIImage imageNamed:@"picbtn_lib.png"] forState:UIControlStateNormal];
        UIButton *camBtn = [[UIButton alloc] initWithFrame:CGRectMake(106, 0, 108, 45)];
        [camBtn setBackgroundImage:[UIImage imageNamed:@"picbtn_cam.png"] forState:UIControlStateNormal];
        UIButton *fbBtn = [[UIButton alloc] initWithFrame:CGRectMake(214, 0, 106, 45)];
        [fbBtn setBackgroundImage:[UIImage imageNamed:@"picbtn_fbook.png"] forState:UIControlStateNormal];

        [libBtn addTarget:self action:@selector(addBtnLib) forControlEvents:UIControlEventTouchUpInside];
        [camBtn addTarget:self action:@selector(addBtnCam) forControlEvents:UIControlEventTouchUpInside];
        if( [FBSession.activeSession isOpen] )
            [fbBtn addTarget:self action:@selector(addBtnFb) forControlEvents:UIControlEventTouchUpInside];

        [btnBar addSubview:libBtn];
        [btnBar addSubview:camBtn];
        [btnBar addSubview:fbBtn];
        [libBtn setAutoresizingMask:UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin];
        [camBtn setAutoresizingMask:UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin];
        [fbBtn setAutoresizingMask:UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleLeftMargin];
    }

    [btnBar setFrame:addBtn.frame];

    modalBtn = [[UIButton alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [modalBtn addTarget:self action:@selector(closeAddBar:) forControlEvents:UIControlEventTouchUpInside];

    [((AppDelegate*)([UIApplication sharedApplication].delegate)).window addSubview:modalBtn];
    [((AppDelegate*)([UIApplication sharedApplication].delegate)).window addSubview:btnBar];

    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView animateWithDuration:0.2 animations:^{
        [btnBar setFrame:CGRectMake(0, 44+20, 320, 45)];
        [btnBar setAlpha:1.0];
    }];
}

- (void) addBtnLib
{
    [modalBtn removeFromSuperview];
    [btnBar removeFromSuperview];
    if( nil == imgPicker )
        imgPicker = [[UIImagePickerController alloc] init];

    [imgPicker setDelegate:self];
    [imgPicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [self presentViewController:imgPicker animated:YES completion:^(void){}];
}

- (void) addBtnCam
{
    [modalBtn removeFromSuperview];
    [btnBar removeFromSuperview];
    if( nil == imgPicker )
        imgPicker = [[UIImagePickerController alloc] init];
    [imgPicker setDelegate:self];
    imgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    imgPicker.mediaTypes = @[(NSString *)kUTTypeImage];
    imgPicker.allowsEditing = NO;
    dispatch_async(dispatch_get_main_queue(), ^(){
        [self presentViewController:imgPicker animated:YES completion:^(void){}];
    });
}

- (void) addBtnFb
{
    [self enableMenuGesture:NO];
    [modalBtn removeFromSuperview];
    [btnBar removeFromSuperview];

    CSFBAlbumTableController *fbAlbumVC = [[CSFBAlbumTableController alloc] init];
    UINavigationController *faNavi = [[UINavigationController alloc] initWithRootViewController:fbAlbumVC];
    [self presentViewController:faNavi animated:YES completion:^{
    }];
}

- (void) closeAddBar:(UIButton*)sender
{
    [sender removeFromSuperview];

    [UIView animateWithDuration:0.2 animations:^{
        [btnBar setFrame:addBtn.frame];
        [btnBar setAlpha:0.5];
    } completion:^(BOOL finished) {
        [btnBar removeFromSuperview];
        [self hideAddButton:NO];
    }];
}

#pragma mark - Image Picker Delegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:^(void){}];
}

// 이미지가 선택되면 여기에서 포스팅 작업을 하자.
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)Info
{
    [picker dismissViewControllerAnimated:YES completion:^(void)
     {
         STOP_WAIT_VIEW;
         
         NewPicViewController *newPicVC = [[NewPicViewController alloc] init];
         [newPicVC setImage:image];
         [self.navigationController pushViewController:newPicVC animated:YES];
     }];
}

#pragma mark -

- (void) insertPhotoUrl:(NSString*)address thumbUrl:(NSString*) thumbAddr
{
    NSDictionary *picDic = @{ @"votes": @[@(0),@(0),@(0),@(0),@(0),@(0)],
    @"url": address, @"sex":@(USERCONTEXT.myGender),
    @"url2": thumbAddr,
    @"id": @"0" };
    [pbList insertObject:picDic atIndex:0];
    
    [self.collectionView reloadData];
}

// 지금 막 등록된 아이템의 아이디 값을 갱신한다.
- (void) uplodedPhotoID:(NSString*) new_id
{
    NSMutableDictionary *bornItem = [[pbList objectAtIndex:0] mutableCopy];
    
    [bornItem setObject:new_id forKey:@"id"];
    
    [pbList setObject:bornItem atIndexedSubscript:0];
    
    // backup to queue.
    [listQueue removeAllEntries];
    [listQueue addEntryWithInfo:@{@"title":@"publicList"}
                      resources:pbList
                        tagName:nil];
    
}

@end
