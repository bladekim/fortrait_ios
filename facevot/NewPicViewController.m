//
//  NewPicViewController.m
//  facevot
//
//  Created by 태한 김 on 12. 7. 4..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//

#import "NewPicViewController.h"
#import "MainViewController.h"
#import "NetInterface.h"

@implementation NewPicViewController

- (id)init
{
    self = [super init];
    if (self) {
        self.title = @"New Photo";
        self.view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 460-44)];

        // Custom initialization
        imgView = [[UIImageView alloc]init];
        [imgView setBackgroundColor:[UIColor scrollViewTexturedBackgroundColor]];
        [imgView setContentMode:UIViewContentModeScaleAspectFit];
        [self.view addSubview:imgView];

        toolBar = [[UIToolbar alloc] init];
        [toolBar setTintColor:[UIColor blackColor]];
        [toolBar setAlpha:0.5];
        [self.view addSubview:toolBar];

        UIBarButtonItem *aviaryItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editAction:)];
        [toolBar setItems:@[aviaryItem]];

        UIBarButtonItem *okItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(confirmAction:)];
        self.navigationItem.rightBarButtonItem = okItem;
    }
    return self;
}

- (void) setImage:(UIImage*) image
{
    [imgView setImage:image];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [imgView setFrame:self.view.bounds];
    [toolBar setFrame:CGRectMake(0, self.view.bounds.size.height-44, 320, 44)];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSUInteger) supportedInterfaceOrientations
{
    return( UIInterfaceOrientationPortrait|
           UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark -

- (void) editAction:(id) sender
{
    AFPhotoEditorController *editorController = [[AFPhotoEditorController alloc] initWithImage:imgView.image];
    [editorController setDelegate:self];
    [self presentViewController:editorController animated:YES completion:NULL];
}

 // 업로드 시작해야 함.
- (void) confirmAction:(id) sender
{
    START_WAIT_VIEW;

    FBRequestConnection *connection = [[FBRequestConnection alloc] init];
    FBRequest *request1;
    UIImage *newImg;

    // 이미지를 그대로 올리자니, 업로드 과정이 느린 우리는 지나친 비효울이 발생한다.
    // 최소한, 원본이미지가 너무 크다면 iPhone 에 적당한 형태로 줄여서 사용하는 것이 좋겠다.
    if( imgView.image.size.width > 640 ){
        newImg = [UserContext imageWithImage:imgView.image
                                scaledToSize:CGSizeMake(640, imgView.image.size.height * (640.0/imgView.image.size.width))];
        request1 = [FBRequest requestForUploadPhoto:newImg];

        NSLog(@"img resizing:(%f,%f)->(%f,%f)",imgView.image.size.width, imgView.image.size.height, newImg.size.width,newImg.size.height);
    }
    else {
        // First request uploads the photo.
        request1 = [FBRequest requestForUploadPhoto:imgView.image];
    }
    NSMutableDictionary *paramdic = request1.parameters;
    [paramdic setObject:@"from Facevot App. Come and vote to my photo!" forKey:@"message"]; // TODO:메시지와 링크를 추가.
    [paramdic setObject:@"http://sites.google.com/site/readobject/" forKey:@"link"];

    [connection addRequest:request1
         completionHandler:
     ^(FBRequestConnection *connection, id result, NSError *error) {
         NSLog(@"req1:%@",result);
         if (!error) {
             NSLog(@"error:%@",error);
         }
     }
            batchEntryName:@"photopost"
     ];
    
    // Second request retrieves photo information for just-created
    // photo so we can grab its source.
    FBRequest *request2 = [FBRequest
                           requestForGraphPath:@"{result=photopost:$.id}"];
    [connection addRequest:request2
         completionHandler:
     ^(FBRequestConnection *connection, id result, NSError *error) {

         if (!error && result)
         {
             // 성공적으로 페이스북에 사진을 올렸으면, 그 URL을 우리 서버에 등록한다.
             // images 에서 4번째 이미지는 긴축 480pixel 이미지.
             NSString *source = [(NSDictionary*)( ((NSArray*)[result objectForKey:@"images"])[3]) objectForKey:@"source"];
             NSString *thumbSource = [(NSDictionary*)( ((NSArray*)[result objectForKey:@"images"])[4] ) objectForKey:@"source"];

             // 새 사진을 올린 후, 되로 돌아가서 전체 목록을 또 읽는 비효율을 절약하기 위해
             // 신규 목록 하나를 리스트 맨 앞에 삽입한다.
             MainViewController *mv = (MainViewController*)(self.navigationController.viewControllers[0]);

             // 되도록 빨리 반응하기 위해서, 페이스북 업로드가 끝나면 화면은 마치 다 된것 처럼 보이게 하고
             // 실제 facevot.com 서버로 데이터를 등록하는 것은 뒤에서 몰래 하고, 그 결과는 다시 갱신한다.
             [NetInterface postMyPhotoURL:source
                                 thumbURL:thumbSource
                               onComplete:^(NSString* newID)
             {
                 dispatch_async(dispatch_get_main_queue(), ^(){
                     [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                     [mv uplodedPhotoID:newID];
                 });
             }
                                  onError:^(NSError *err)
             {
                 [self.navigationController popViewControllerAnimated:NO];
                 NOTI(@"information is not updated.");
                 [mv removeLatestItem];
             }];

             dispatch_async(dispatch_get_main_queue(), ^(){
                 STOP_WAIT_VIEW;
                 [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
                 [self.navigationController popViewControllerAnimated:YES];

                 [mv insertPhotoUrl:source thumbUrl:thumbSource];
             });
         }
         else
         {
             STOP_WAIT_VIEW;
             NOTI(@"Upload Error.\nTry on next time.");
         }
     }
     ];
    
    [connection start];
}

#pragma mark - AFPhotoEditDelegate

- (void)photoEditor:(AFPhotoEditorController *)editor finishedWithImage:(UIImage *)image
{
    [imgView setImage:image];
    [editor dismissViewControllerAnimated:YES completion:NULL];
}

- (void)photoEditorCanceled:(AFPhotoEditorController *)editor
{
    // Handle cancelation here
    [editor dismissViewControllerAnimated:YES completion:NULL];
}

@end
