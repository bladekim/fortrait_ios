//
//  PhotoViewController.m
//  ImInHotspot
//
//  Created by 태한 김 on 11. 3. 3..
//  Copyright 2011 kth. All rights reserved.
//

#import "PhotoViewController.h"
#import "ImageScrollView.h"
#import "AppDelegate.h"
#import "PCPieChart.h"
#import "TSPopoverController.h"
#import "TSActionSheet.h"
#import "NetInterface.h"

#define PADDING  0

@implementation PhotoViewController


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)loadView
{
    // Step 1: make the outer paging scroll view
    pagingScrollView = [[UIScrollView alloc] initWithFrame:[self frameForPagingScrollView]];
    pagingScrollView.pagingEnabled = YES;
    pagingScrollView.backgroundColor = [UIColor blackColor];
    pagingScrollView.showsVerticalScrollIndicator = NO;
    pagingScrollView.showsHorizontalScrollIndicator = NO;
    pagingScrollView.contentSize = [self contentSizeForPagingScrollView];
    pagingScrollView.directionalLockEnabled = YES;
    pagingScrollView.alwaysBounceHorizontal = YES;
    pagingScrollView.delegate = self;
    self.view = pagingScrollView;

    // Step 2: prepare to tile content
    recycledPages = [[NSMutableSet alloc] init];
    visiblePages  = [[NSMutableSet alloc] init];

    // Header
    head = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 20, 320, 44)];
    [head setBackgroundImage:[UIImage imageNamed:@"navigationBar.png"] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
    [head setAlpha:0.7];

    // 테두리 없는 버튼 아이템을 만든다.
    UIButton *bBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 22)];
    [bBtn setBackgroundImage:[UIImage imageNamed:@"backBtn.png"] forState:UIControlStateNormal];
    [bBtn addTarget:self action:@selector(closeAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:bBtn];

    UIBarButtonItem *flexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];

    UIButton *sBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 22)];
    [sBtn setBackgroundImage:[UIImage imageNamed:@"shareBtn.png"] forState:UIControlStateNormal];
    [sBtn addTarget:self action:@selector(shareAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *shareBtn = [[UIBarButtonItem alloc] initWithCustomView:sBtn];

    [head setItems:@[backItem,flexible,shareBtn]];

    nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 0, 200, 43)];
    [nameLabel setBackgroundColor:[UIColor clearColor]];
    [nameLabel setTextColor:[UIColor blackColor]];
    [nameLabel setFont:CS_BOLD_FONT(14.0)];
    [nameLabel setTextAlignment:UITextAlignmentCenter];
    [head addSubview:nameLabel];

    // 투표하거나 결과를 알 수 있는 버튼
    UIBarButtonItem *voteItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"t_vote.png"] style:UIBarButtonItemStylePlain target:self action:@selector(btnAction:)];

    UIBarButtonItem *fbItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"t_fbook.png"] style:UIBarButtonItemStylePlain target:self action:@selector(showFacebookPage)];
//    if( [dicInfo[@"identity"] isEqualToString:USERCONTEXT.myIdentity] )
//        fbItem.enabled = NO;  // 나 자신은 필요 없지 않나?

    UIBarButtonItem *favoItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"t_favo.png"] style:UIBarButtonItemStylePlain target:self action:@selector(favorateBtnAction:)];

    //Footer
    tail = [[UIToolbar alloc]initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height-51, 320, 51)];
    [tail setBackgroundImage:[UIImage imageNamed:@"toolBar.png"] forToolbarPosition:UIToolbarPositionBottom barMetrics:UIBarMetricsDefault];
    [tail setAlpha:0.7];
    [tail setItems:@[voteItem,fbItem,favoItem]];

    [((AppDelegate*)[UIApplication sharedApplication].delegate).window addSubview:head];
    [((AppDelegate*)[UIApplication sharedApplication].delegate).window addSubview:tail];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(imageTab)
                                                 name:@"NOTI_IMAGE_TAP"
                                               object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
/*
    [self tilePages];

    // Announce the index to the user
    int firstNeededPageIndex = floorf(CGRectGetMinX(self.view.bounds) / CGRectGetWidth(self.view.bounds));
    if( firstNeededPageIndex < 0 ) firstNeededPageIndex = 0;
    if( firstNeededPageIndex == [_listDics count] ) firstNeededPageIndex--;
    
    // identity 값이 같으면, 나 자신의 사진을 보는 경우로 간주하고 내 이름을 보여준다.
    if( [_listDics[firstNeededPageIndex][@"identity"] isEqualToString:USERCONTEXT.myIdentity] )
    {
        [nameLabel setText:USERCONTEXT.myName];
        [tail.items[1] setEnabled:NO];
    }else {
        // 낮선 사람이라면 페이스북에 identity 를 조회해서 정보를 얻는다.
        [FBRequest startWithGraphPath:_listDics[firstNeededPageIndex][@"identity"]
                    completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                        [nameLabel setText: [result objectForKey:@"name"]];
                        facebookLink = [result objectForKey:@"link"];
                        [tail.items[1] setEnabled:YES];
                    }];
    }
*/
    [self scrollViewDidScroll:pagingScrollView];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return NO;
}

-(void)setStartIndex:(NSUInteger)index
{
    currentIdx = index;
    [pagingScrollView setContentOffset:CGPointMake(320*index,0) animated:NO];
}

#pragma mark - ScrollView delegate methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self tilePages];

    // Announce the index to the user
    int firstNeededPageIndex = floorf((CGRectGetMinX(scrollView.bounds)+100) / CGRectGetWidth(scrollView.bounds));
    if( firstNeededPageIndex < 0 ) firstNeededPageIndex = 0;
    if( firstNeededPageIndex == [_listDics count] ) firstNeededPageIndex--;

    // identity 값이 같으면, 나 자신의 사진을 보는 경우로 간주하고 내 이름을 보여준다.
    if( [_listDics[firstNeededPageIndex][@"identity"] isEqualToString:USERCONTEXT.myIdentity] )
    {
        [nameLabel setText:USERCONTEXT.myName];
        [tail.items[1] setEnabled:NO];
    } else {
        // 낮선 사람이라면 페이스북에 identity 를 조회해서 정보를 얻는다.
//        [FBRequest startWithGraphPath:_listDics[firstNeededPageIndex][@"identity"]
//                    completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//                        [nameLabel setText: [result objectForKey:@"name"]];
//                        facebookLink = [result objectForKey:@"link"];
//                        [tail.items[1] setEnabled:YES];
//                    }];
        FBRequest *req = [FBRequest requestForGraphPath:_listDics[firstNeededPageIndex][@"identity"]];
        [req startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            [nameLabel setText: [result objectForKey:@"name"]];
            facebookLink = [result objectForKey:@"link"];
            [tail.items[1] setEnabled:YES];
        }];

    }

    currentIdx = firstNeededPageIndex;

    if( [_listDics[firstNeededPageIndex][@"vote"] isEqualToNumber:@(1)] ) // 투표 했으면 버튼은 챠트 보기용.
        [(UIBarButtonItem*)(tail.items[0]) setImage:[UIImage imageNamed:@"t_chart.png"]];
    else
        [(UIBarButtonItem*)(tail.items[0]) setImage:[UIImage imageNamed:@"t_vote.png"]];

    if( _listDics[firstNeededPageIndex][@"favorite"] ) // 즐겨찾기 했으면 버튼은 해제용.
        [(UIBarButtonItem*)(tail.items[2]) setImage:[UIImage imageNamed:@"t_unfavo.png"]];
    else
        [(UIBarButtonItem*)(tail.items[2]) setImage:[UIImage imageNamed:@"t_favo.png"]];
}

#pragma mark -

- (void)tilePages
{
    // Calculate which pages are visible
    CGRect visibleBounds = pagingScrollView.bounds;
    int firstNeededPageIndex = floorf(CGRectGetMinX(visibleBounds) / CGRectGetWidth(visibleBounds));
    int lastNeededPageIndex  = floorf((CGRectGetMaxX(visibleBounds)-1) / CGRectGetWidth(visibleBounds));
    firstNeededPageIndex = MAX(firstNeededPageIndex, 0);
    lastNeededPageIndex  = MIN(lastNeededPageIndex, [_listDics count] - 1);
    
    // Recycle no-longer-visible pages 
    for (ImageScrollView *page in visiblePages) {
        if (page.index < firstNeededPageIndex || page.index > lastNeededPageIndex) {
            [recycledPages addObject:page];
            [page cancelImageLoading];  // blade.
            [page removeFromSuperview];
        }
    }
    [visiblePages minusSet:recycledPages];
    
    // add missing pages
    for (int index = firstNeededPageIndex; index <= lastNeededPageIndex; index++) {
        if (![self isDisplayingPageForIndex:index]) {
            ImageScrollView *page = [self dequeueRecycledPage];
            if (page == nil) {
                page = [[ImageScrollView alloc] initWithFrame:CGRectMake(0,0,320,480)];
            }
            [self configurePage:page forIndex:index];
            [pagingScrollView addSubview:page];
            [visiblePages addObject:page];
        }
    }
}

- (ImageScrollView *)dequeueRecycledPage
{
    ImageScrollView *page = [recycledPages anyObject];
    if (page) {
        [recycledPages removeObject:page];
    }
    return page;
}

- (BOOL)isDisplayingPageForIndex:(NSUInteger)index
{
    BOOL foundPage = NO;
    for (ImageScrollView *page in visiblePages) {
        if (page.index == index) {
            foundPage = YES;
            break;
        }
    }
    return foundPage;
}

- (void)configurePage:(ImageScrollView *)page forIndex:(NSUInteger)index
{
    page.index = index;
    page.frame = [self frameForPageAtIndex:index];

    // Use tiled images
    [page displayImage:[_listDics[index] objectForKey:@"url"]];
//    NSLog(@"[%@]",[[imageDics objectAtIndex:index] objectForKey:@"thumbUrl"]);
}

#pragma mark - Frame calculations

- (CGRect)frameForPagingScrollView {
    CGRect frame = CGRectMake(0, 20, 320, [UIScreen mainScreen].bounds.size.height-20);
    frame.origin.x -= PADDING;
    frame.size.width += (2 * PADDING);
    return frame;
}

- (CGRect)frameForPageAtIndex:(NSUInteger)index {
    // We have to use our paging scroll view's bounds, not frame, to calculate the page placement. When the device is in
    // landscape orientation, the frame will still be in portrait because the pagingScrollView is the root view controller's
    // view, so its frame is in window coordinate space, which is never rotated. Its bounds, however, will be in landscape
    // because it has a rotation transform applied.
    CGRect bounds = pagingScrollView.bounds;
    CGRect pageFrame = bounds;
    pageFrame.size.width -= (2 * PADDING);
    pageFrame.origin.x = (bounds.size.width * index) + PADDING;
    return pageFrame;
}

- (CGSize)contentSizeForPagingScrollView {
    // We have to use the paging scroll view's bounds to calculate the contentSize, for the same reason outlined above.
    CGRect bounds = pagingScrollView.bounds;
    NSLog(@"paging bounds height : %f", CGRectGetHeight(pagingScrollView.bounds));
    return CGSizeMake(bounds.size.width * [_listDics count] - (PADDING*2*[_listDics count]), bounds.size.height - 3);
}


//- (NSUInteger)imageCount
//{
//    return [_listDics count];
//}

#pragma mark - Navigation Bar

- (void) imageTab
{
    [UIView animateWithDuration:0.3 animations:^{
        if( 0.7f == head.alpha ){
            head.alpha = 0.0;
            tail.alpha = 0.0;
        }else{
            head.alpha = 0.7f;
            tail.alpha = 0.7f;
        }
    }];
}

-(void) closeAction:(id)sender
{
    [head removeFromSuperview];
    [tail removeFromSuperview];
    [[self navigationController] setNavigationBarHidden:NO animated:NO];

	[self.navigationController popViewControllerAnimated:YES];
}

- (void) shareAction
{
    UIActivityViewController *avc = [[UIActivityViewController alloc] initWithActivityItems:@[_listDics[currentIdx][@"url"],@"address"]
                                                                      applicationActivities:nil];
    [self presentViewController:avc animated:YES completion:^(void){

    }];
}

#pragma mark - Vote Query

// 내가 투표했는지 알아보고, 투표했으면 현황을 보여주고, 그렇지 않다면 투표 할 수 있게 해준다.
// 버튼을 누르면 여기로.
-(void) btnAction:(UIButton*)sender
{
    if( [_listDics[currentIdx][@"id"] isEqualToString:@"0"] ){  // 아직 id가 없다.
        [self displayCanNotVoteNow];
        return;
    }

    if( [_listDics[currentIdx][@"vote"] isEqualToNumber:@(1)] )
        [self displayChartOnPannel];   // 했다.
    else
        [self displayVoteBoard];       // 안했다.
}

// 사진의 사람에 대한 즐겨찾기 토글 버튼.
-(void) favorateBtnAction:(UIButton*) sender
{
    BOOL remember;
    if( nil == USERCONTEXT.myIdentity )
    {
        // 로그인 되지 않은 상태라면, 로그인을 하고 성공했으면 다시 재귀호출하여
        // 좋아요 등록/삭제 작업을 한다.
//        if( ![NetInterface facebookLogin:^(BOOL result)
//              {
//                  if( result ){
//                      [self favorateBtnAction:nil];
//                  }
//              } showUI:NO] )
        {
            [NetInterface facebookLogin:^(BOOL result)
             {
                 if( result ){
                     [self favorateBtnAction:nil];
                 }
             } showUI:YES];
        }
        return;
    }

    [sender setUserInteractionEnabled:NO];
    if( _listDics[currentIdx][@"favorite"] )
    {      // 해지.
        remember = NO;
    } else {                // 기억.
        remember = YES;
    }

    [NetInterface setFavoriteManId:_listDics[currentIdx][@"identity"]
                      withPhottoId:_listDics[currentIdx][@"id"]
                         boolValue:remember
                              name:[nameLabel.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                        onComplete:^(BOOL result)
     {
         if( !remember )
             [(UIBarButtonItem*)(tail.items[2]) setImage:[UIImage imageNamed:@"t_favo.png"]];
         else
             [(UIBarButtonItem*)(tail.items[2]) setImage:[UIImage imageNamed:@"t_unfavo.png"]];

         // 이 값이 참이면 즐겨찾기 목록 뷰 화면으로 갈 때 자동으로 리로딩한다.
         USERCONTEXT.needReloadFavo = YES;

         // 새로 바뀐 정보를 가지고 있는 데이터에 반영한다.
         NSMutableDictionary *newDic = [[NSMutableDictionary alloc] initWithDictionary:_listDics[currentIdx]];
         NSNumber *newVal = [NSNumber numberWithBool:remember];
         [newDic setObject:newVal forKey:@"favorites"];

         [_listDics replaceObjectAtIndex:currentIdx withObject:newDic];

         [USERCONTEXT completeView];
         [sender setEnabled:YES];
     }
                           onError:^()
     {
         NOTI(@"Network Error");
         [sender setEnabled:YES];
     }];
}

-(void) showFacebookPage
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:facebookLink]];
}


// 선택한 표를 던진다.
-(void) doVote:(NSUInteger) choice
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

    [tail.items[0] setEnabled:NO];

    [NetInterface doVoteAtPhotoID:_listDics[currentIdx][@"id"] vote:choice
                       onComplete:^
     {
         // 새로 바뀐 정보를 가지고 있는 데이터에 반영한다.
         NSMutableDictionary *newDic = [[NSMutableDictionary alloc] initWithDictionary:_listDics[currentIdx]];
         [newDic setValue:@(1) forKey:@"vote"];
         NSUInteger nm = [(NSNumber*)(newDic[@"votes"][choice]) integerValue] + 1;
         NSMutableArray *newVotes = [[NSMutableArray alloc] initWithArray:newDic[@"votes"]];
         [newVotes replaceObjectAtIndex:choice withObject:@(nm)];
         [newDic setObject:newVotes forKey:@"votes"];

         [_listDics replaceObjectAtIndex:currentIdx withObject:newDic];

         dispatch_async(dispatch_get_main_queue(), ^(){
             NSLog(@"vote OK.");
             NSString *msg = [NSString stringWithFormat:@"%@ is %@!",([_listDics[currentIdx][@"sex"] integerValue]?@"She":@"He"), ITEM_TXT([_listDics[currentIdx][@"sex"] integerValue])[choice] ];
             [[[UIAlertView alloc] initWithTitle:@"OK" message:msg delegate:nil cancelButtonTitle:@"Confirm" otherButtonTitles:nil] show];

             [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

             // 차트 보기 버튼으로 표시 변경.
             [(UIBarButtonItem*)(tail.items[0]) setImage:[UIImage imageNamed:@"t_chart.png"]];
             [tail.items[0] setEnabled:YES];
         });
     } onError:^{
         dispatch_async(dispatch_get_main_queue(), ^(){
             NOTI(@"Can not vote now.");
             [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
             [tail.items[0] setEnabled:YES];
         });
     }];
}

-(void) displayCanNotVoteNow
{
    UILabel *msg = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 180, 60)];
    [msg setTextAlignment:NSTextAlignmentCenter];
    [msg setBackgroundColor:[UIColor clearColor]];
    [msg setTextColor:[UIColor lightGrayColor]];
    [msg setShadowColor:[UIColor blackColor]];
    [msg setShadowOffset:CGSizeMake(0, 1)];
    [msg setFont:CS_FONT(11)];
    [msg setText:@"Oops,\nplease try next time."];

    UIViewController *msgCtrl = [[UIViewController alloc] init];
    [msgCtrl setView:msg];
    TSPopoverController *popCtrl = [[TSPopoverController alloc] initWithContentViewController:msgCtrl];
    [popCtrl showPopoverWithRect:CGRectMake(0, [UIScreen mainScreen].bounds.size.height-40, 50, 20)];
};

-(void) displayChartOnPannel
{
#define POPOVER_X 20
    
    NSInteger sex = [_listDics[currentIdx][@"sex"] integerValue];
    
    VoteResultView *voteResultView = [[VoteResultView alloc] initWithStyle:(sex ? VoteResultViewForFemale:VoteResultViewForMale)];
    voteResultView.dataSource = self;
    
    TSPopoverController *popCtrl = [[TSPopoverController alloc] initWithView:voteResultView];
    popCtrl.popoverBaseColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    [popCtrl showPopoverWithRect:CGRectMake(POPOVER_X, [UIScreen mainScreen].bounds.size.height-40, 50, 20)];
}

-(void) displayVoteBoard
{
    TSActionSheet *voteActSheet = [[TSActionSheet alloc] initWithTitle:[NSString stringWithFormat:@"%@ is...",([_listDics[currentIdx][@"sex"] integerValue]?@"She":@"He")]];
    [voteActSheet setBackgroundColor:CS_RGB(20, 20, 20)];

    for(NSUInteger i = 0; i <= 5; i++ ){
        [voteActSheet addButtonWithTitle:ITEM_TXT([_listDics[currentIdx][@"sex"] integerValue])[i]
                                   color:[UIColor clearColor]
                              titleColor:ITEM_COR[i]
                             borderWidth:1.0 borderColor:[UIColor blackColor] block:^{
                                 [self doVote:i];
                             }];
    }
    [voteActSheet showWithRect:CGRectMake(0, [UIScreen mainScreen].bounds.size.height-40, 50, 20)];
}

#pragma mark - Vote Reulst View Data Source

// get voting data for vote result view
- (NSInteger)voteResultView:(VoteResultView*)view voteCountsForIndex:(NSInteger)index
{
    NSArray *votes = _listDics[currentIdx][@"votes"];
    return ((NSNumber*)votes[index]).integerValue;
}

@end
