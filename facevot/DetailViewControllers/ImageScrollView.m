//
//  ImageScrollView.m
//  ImInHotspot
//
//  Created by 태한 김 on 11. 3. 3..
//  Copyright 2011 kth. All rights reserved.
//

#import "ImageScrollView.h"

@implementation ImageScrollView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.delegate = self;
        self.showsVerticalScrollIndicator = NO;
        self.showsHorizontalScrollIndicator = NO;
        self.zoomScale = 1.0;
        self.multipleTouchEnabled = YES;
        self.scrollEnabled = NO;
        self.bounces = NO;
    }
    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return ( interfaceOrientation == UIInterfaceOrientationPortrait ||
            interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown );
}

#pragma mark -

-(void)displayImage:(NSString *)imageUrl
{
    [imageView removeFromSuperview];
    imageView = nil;

    imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height-20)];
    [imageView setContentMode:UIViewContentModeScaleAspectFit];
    [self addSubview:imageView];
    [imageView setImageWithURL:[NSURL URLWithString:imageUrl]];
    self.contentSize = [imageView.image size];
//    [self setMaxMinZoomScalesFor
    self.zoomScale = self.minimumZoomScale;
    self.maximumZoomScale = 2.0;
}

-(void)cancelImageLoading
{
    if( nil != imageView )
        [imageView cancelImageRequestOperation];
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    // 한 손가락으로 한번 탭핑한 경우 판별.
    if( 1 == touches.count && 1 == [[touches anyObject] tapCount] )
    {
		[[NSNotificationCenter defaultCenter] postNotificationName:@"NOTI_IMAGE_TAP" object:nil];
    }
}

@end
