//
//  VoteResultView.h
//  fortrait
//
//  Created by Wonil Kim on 9/29/12.
//  Copyright (c) 2012 ChocolateSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VoteResultView;

/*
 Data source of VoteResultView
*/

@protocol VoteResultViewDataSource <NSObject>

@required

// get vote counts for vote type at index
- (NSInteger)voteResultView:(VoteResultView*)view voteCountsForIndex:(NSInteger)index;

@end

/*
 Delegate of VoteResultView
*/

@protocol VoteResultViewDelegate <NSObject>

@required

@optional

// get color of vote type at index
- (UIColor*)voteResultView:(VoteResultView*)view colorForIndex:(NSInteger)index;

// get icon of vote type at index
- (UIImage*)voteResultView:(VoteResultView*)view iconForIndex:(NSInteger)index;

@end

typedef enum _VoteResultViewStyle
{
    VoteResultViewForMale = 1,
    VoteResultViewForFemale,
} VoteResultViewStyle;

/*
 VoteResultView class shows voting result as pie graph and list
 User can turns on/off list porting and pie graph portion UI
 Also, user can customize color for each legend
*/

@interface VoteResultView : UIView

- (id)initWithStyle:(VoteResultViewStyle)aStyle;

@property (nonatomic, readonly) VoteResultViewStyle style;
@property (nonatomic, assign) id<VoteResultViewDataSource> dataSource;
@property (nonatomic, assign) id<VoteResultViewDelegate> delegate;

@end
