//
//  StageCell.m
//  facevot
//
//  Created by 태한 김 on 12. 9. 25..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//

#import "StageCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation StageCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        picView = [[UIImageView alloc] initWithFrame:CGRectMake(1,1,self.bounds.size.width-2,self.bounds.size.height-2)];
        [picView setClipsToBounds:YES];
        [self.contentView addSubview:picView];
        picView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        [picView setContentMode:UIViewContentModeScaleAspectFill];
        [picView setUserInteractionEnabled:YES];

        favoMark = nil;

        [self setAutoresizesSubviews:YES];
        [self setBackgroundColor:THEME_GRAY];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void) setImageURL:(NSString*)url
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if( ![url isKindOfClass:[NSNull class]] )
            [picView setImageWithURL:[NSURL URLWithString:url]];
    });
}

-(void) setFavoriteMark:(BOOL) mark
{
    if( nil == favoMark )
    {
        favoMark = [[UIImageView alloc] initWithFrame:CGRectMake(9, -2, 15, 20)];
        [favoMark setImage:[UIImage imageNamed:@"favoTag.png"]];
        [self addSubview:favoMark];
    }

    [favoMark setAlpha: (mark) ? 1.0 : 0.0 ];
}

@end
