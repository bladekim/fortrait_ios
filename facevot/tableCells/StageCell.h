//
//  StageCell.h
//  facevot
//
//  Created by 태한 김 on 12. 9. 25..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StageCell : UICollectionViewCell
{
    UIImageView *picView;
    UIImageView *favoMark;
}

-(void) setImageURL:(NSString*)url;
-(void) setFavoriteMark:(BOOL) mark;

@end
