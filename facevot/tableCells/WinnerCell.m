//
//  WinnerCell.m
//  facevot
//
//  Created by 태한 김 on 12. 7. 2..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//

#import "WinnerCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation WinnerCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        CGFloat wg = (213/3);
        // Initialization code
        UIImageView *kView = [[UIImageView alloc] initWithFrame:CGRectMake(1, 1, 106, WINNER_CELL_H-2)];
        UIImageView *wView1 = [[UIImageView alloc] initWithFrame:CGRectMake(108, 1, wg-2, (WINNER_CELL_H/2)-2)];
        UIImageView *wView2 = [[UIImageView alloc] initWithFrame:CGRectMake(108+wg, 1, wg-2, (WINNER_CELL_H/2)-2)];
        UIImageView *wView3 = [[UIImageView alloc] initWithFrame:CGRectMake(108+(wg*2), 1, wg-2, (WINNER_CELL_H/2)-2)];
        UIImageView *wView4 = [[UIImageView alloc] initWithFrame:CGRectMake(108, (WINNER_CELL_H/2), wg-2, (WINNER_CELL_H/2)-1)];
        UIImageView *wView5 = [[UIImageView alloc] initWithFrame:CGRectMake(108+wg, (WINNER_CELL_H/2), wg-2, (WINNER_CELL_H/2)-1)];
        UIImageView *wView6 = [[UIImageView alloc] initWithFrame:CGRectMake(108+(wg*2), (WINNER_CELL_H/2), wg-2, (WINNER_CELL_H/2)-1)];

        imgArray = @[ wView1, wView2, wView3, wView4, wView5, wView6, kView ];
        NSUInteger i = 0;

        for( UIImageView *v in imgArray ) {
            v.layer.borderColor = ((UIColor*)(ITEM_COR[i])).CGColor;
            v.layer.borderWidth = 1.0f;
            [v setClipsToBounds:YES];
            [v setContentMode:UIViewContentModeScaleAspectFill];

            [self.contentView addSubview:v];

            UIButton *tbtn = [[UIButton alloc] initWithFrame:v.frame];
            [tbtn setTag:i];
            [tbtn addTarget:self action:@selector(tabImg:) forControlEvents:UIControlEventTouchUpInside];
            [self.contentView addSubview:tbtn];
            i++;
        }

        [self.selectedBackgroundView setBackgroundColor:[UIColor darkGrayColor]];
    }

    rate = [[MKNumberBadgeView alloc] initWithFrame:CGRectMake(3, 3, 40, 20)];
    [rate setAlignment:NSTextAlignmentLeft];
    [rate setFillColor:[UIColor orangeColor]];
    [self.contentView addSubview:rate];

    return self;
}

-(void) setImageDic:(NSDictionary*)data index:(NSUInteger)idx;
{
    index = idx;

    // 6개의 작은 이미지는 정말 작은 이미지로 url을 교체해서 사용.
    for( NSUInteger i = 0; i <= 5; i ++ ) {
        NSUInteger len = [[data[@(i)] objectForKey:@"url2"] length];
        if( 5 > len ){
            [imgArray[i] setTag:44];  // no img.
            [(UIImageView*)(imgArray[i]) setImage:[UIImage imageNamed:@"no_img_01.png"]];
            continue;
        }
        NSString *surl = [[data[@(i)] objectForKey:@"url2"] stringByReplacingCharactersInRange:NSMakeRange(len-5, 5) withString:@"a.jpg"];
        [imgArray[i] setImageWithURL:[NSURL URLWithString:surl]];
        [imgArray[i] setTag:0];
    }
    // 항목 대표 우승자.
    [(UIImageView*)(imgArray[6]) setImageWithURL:[NSURL URLWithString:[data[@(6)] objectForKey:@"url2"]]];

    // 임시 순위 표시.
    [rate setValue:index + 1];
    [rate setNeedsDisplay];
}

- (void) tabImg:(UIButton*) sender
{
    if( 44 == [imgArray[sender.tag] tag] ) return; // if no img, do nothing.
 
    if( self.delegate != nil && [self.delegate respondsToSelector:@selector(selectedWinnerCell:thumbIndex:thumbView:)] )
        [self.delegate selectedWinnerCell:index thumbIndex:sender.tag thumbView:imgArray[sender.tag]];
}

@end
