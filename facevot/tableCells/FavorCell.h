//
//  FavorateCell.h
//  facevot
//
//  Created by 김태한 on 12. 6. 30..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//
//  즐겨 찾기로 등록된 사람들을 보여주는 테이블에서 사용할 셀 객체.

#import <UIKit/UIKit.h>

@interface FavorCell : UITableViewCell
{
    UILabel *nameLabel;
}

-(void) setImageURL:(NSString*)url name:(NSString*)herName;
@property (nonatomic, strong) UIImageView *imgView;
@property (nonatomic, strong) CIDetector *detector;

@end
