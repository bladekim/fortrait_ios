//
//  WinnerCell.h
//  facevot
//
//  Created by 태한 김 on 12. 7. 2..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MKNumberBadgeView.h"

@interface WinnerCell : UITableViewCell
{
    NSUInteger  index;
    NSArray     *imgArray;
    NSDictionary *dataDic;
    MKNumberBadgeView *rate;
}

-(void) setImageDic:(NSDictionary*)data index:(NSUInteger)index;

@property (nonatomic, assign) id delegate;

@end

@protocol WinnderCellDelegate

// idx:이 셀의 인덱스(순위) tIdx:7개 중 하나. view:선택된 이미지 뷰.
- (void) selectedWinnerCell:(NSUInteger)idx thumbIndex:(NSUInteger)tIdx thumbView:(UIImageView*)view;

@end