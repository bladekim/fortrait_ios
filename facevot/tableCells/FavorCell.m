//
//  FavorateCell.m
//  facevot
//
//  Created by 김태한 on 12. 6. 30..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//

#import "FavorCell.h"
#import <CoreImage/CoreImage.h>

@implementation FavorCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if ( self == nil )
        return ( nil );

    _imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, FAVO_CELL_H)];
    [_imgView setContentMode:UIViewContentModeScaleAspectFill];
    [self addSubview:_imgView];
    [self setClipsToBounds:YES];

    nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, FAVO_CELL_H-40, 320, 40)];
    [nameLabel setBackgroundColor:CS_RGBA(0, 0, 0, 0.6)];
    [nameLabel setFont:CS_BOLD_FONT(20.0)];
    [nameLabel setShadowColor:[UIColor blackColor]];
    [nameLabel setShadowOffset:CGSizeMake(0, 1)];
    [nameLabel setTextColor:[UIColor whiteColor]];
    [self addSubview:nameLabel];

    UIImageView *acView = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width - 30, FAVO_CELL_H/2-12, 24, 24)];
    [acView setImage:[UIImage imageNamed:@"cell_arrow.png"]];
    [self addSubview:acView];

    _detector = [CIDetector detectorOfType:CIDetectorTypeFace
                                  context:nil
                                  options:[NSDictionary dictionaryWithObject:@"CIDetectorAccuracyLow" forKey:@"CIDetectorAccuracy"]];

    return self;
}

-(void) setImageURL:(NSString*)url name:(NSString *)herName
{
    [nameLabel setText:[NSString stringWithFormat:@"  %@",herName]];

    [_imgView setFrame:CGRectMake(0, 0, 320, FAVO_CELL_H)];

    [_imgView setImageWithURL:[NSURL URLWithString:url] placeholderImage:nil
                     success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
    {
        CGImageRef rref = [image CGImage];
        NSArray *fs = [self.detector featuresInImage:[CIImage imageWithCGImage:rref] options:nil];
        if( 0 == [fs count] ) return;

        CGRect faceRect = [(CIFaceFeature*)(fs[0]) bounds];

        CGFloat oy = ((image.size.height * [[UIScreen mainScreen] scale] - (faceRect.origin.y + faceRect.size.height)))  / [[UIScreen mainScreen] scale];
        CGFloat my = (((image.size.height * [[UIScreen mainScreen] scale]) - FAVO_CELL_H) / 2) / [[UIScreen mainScreen] scale];
        NSLog(@"f[%f %f   - img_h %f", oy, my, image.size.height );

        if( oy > my ) return;
    
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:0.5 animations:^{
                [self.imgView setFrame:CGRectOffset(_imgView.frame, 0, my-oy)];
            } completion:^(BOOL finished) {
                ;
            }];
        });

    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
    {
        ;
    }];
}

@end
