//
//  WinnerViewController.h
//  facevot
//
//  Created by 태한 김 on 12. 7. 2..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//

#import "BaseViewController.h"
#import "JCMSegmentPageController.h"

@interface WinnerViewController : BaseViewController <JCMSegmentPageControllerDelegate>
{
    UIButton *genderBtn;
    NSArray *vArray;
    JCMSegmentPageController *segmentPageController;
}

-(void) pushDetailViewCtrl:(UIViewController*) ctrl;

@end
