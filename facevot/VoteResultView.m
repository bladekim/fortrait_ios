//
//  VoteResultView.m
//  fortrait
//
//  Created by Wonil Kim on 9/29/12.
//  Copyright (c) 2012 ChocolateSoft. All rights reserved.
//

#import "VoteResultView.h"

#define DEFAULT_VOTE_TYPES_COUNT 6
#define CUTE_IMG [UIImage imageNamed:@"ig_cute.png"]
#define SEXY_IMG [UIImage imageNamed:@"ig_sexy.png"]
#define GORGEOUS_IMG [UIImage imageNamed:@"ig_gorgeous.png"]
#define PRETTY_IMG [UIImage imageNamed:@"ig_pretty.png"]
#define LOVELY_IMG [UIImage imageNamed:@"ig_lovely.png"]
#define BEAUTIFUL_IMG [UIImage imageNamed:@"ig_beauty.png"]

#define SEXY_MAN_IMG [UIImage imageNamed:@"im_sexy.png"]
#define SWEET_IMG [UIImage imageNamed:@"im_sweet.png"]
#define COOL_IMG [UIImage imageNamed:@"im_cool.png"]
#define HANDSOME_IMG [UIImage imageNamed:@"im_handsome.png"]
#define TOUGH_IMG [UIImage imageNamed:@"im_tough.png"]
#define STRONG_IMG [UIImage imageNamed:@"im_strong.png"]

/*
 Pie component for graph drawing
 */
@interface PieComponent : NSObject

- (id)initWithValue:(float)value color:(UIColor*)color;

@property (nonatomic) float value;
@property (nonatomic) float startDeg;
@property (nonatomic) float endDeg;
@property (nonatomic) UIColor *color;

@end

@implementation PieComponent
@synthesize value, color, startDeg, endDeg;

- (id)initWithValue:(float)aValue color:(UIColor*)aColor
{
    self = [super init];
    if (self) {
        self.value = aValue;
        self.color = aColor;
    }
    return self;
}

+ (id)pieComponentWithValue:(float)value color:(UIColor*) color
{
    return [[super alloc] initWithValue:value color:color];
}

@end

@implementation VoteResultView
{
    NSArray *titles;
    NSArray *icons;
    UIImage *chartBackground;
    NSMutableArray *values;
    NSMutableArray *components;

    NSInteger totalVotes;
    NSInteger maxValue;
    NSInteger maxIndex;
    NSInteger diameter;
}

@synthesize style = _style;

- (id)initWithStyle:(VoteResultViewStyle)aStyle
{
#define FRAME_X 16
#define FRAME_WIDTH 290
#define FRAME_HEIGHT 180

    self = [super initWithFrame:CGRectMake(FRAME_X, 0, FRAME_WIDTH, FRAME_HEIGHT)];
    if (self) {
        _style = aStyle;
    }
    
    if (_style == VoteResultViewForMale) {
        titles = [NSArray arrayWithObjects:@"Sweet", @"Cool", @"Handsome", @"Sexy", @"Tough", @"Strong", nil];
        icons = [NSArray arrayWithObjects:SWEET_IMG, COOL_IMG, HANDSOME_IMG, SEXY_MAN_IMG, TOUGH_IMG, STRONG_IMG, nil];
        chartBackground = [UIImage imageNamed:@"pie_ring_man.png"];
    } else {
        titles = [NSArray arrayWithObjects:@"Sexy", @"Cute", @"Gorgeous", @"Pretty", @"Lovely", @"Beautiful", nil];
        icons = [NSArray arrayWithObjects:SEXY_IMG, CUTE_IMG, GORGEOUS_IMG, PRETTY_IMG, LOVELY_IMG, BEAUTIFUL_IMG, nil];
        chartBackground = [UIImage imageNamed:@"pie_ring_girl.png"];
    }
    
    self.backgroundColor = [UIColor clearColor];
    
    return self;
}

- (void)dealloc
{
    titles = nil;
    icons = nil;
    chartBackground = nil;
    values = nil;
    components = nil;
}

- (void)drawRect:(CGRect)rect
{
    const NSArray *colors = @[
        COLOR_0, COLOR_1, COLOR_2, COLOR_3, COLOR_4, COLOR_5
    ];
    
    // initialize vote values from data source
    if (self.dataSource) {
        id<VoteResultViewDataSource> dataSource = self.dataSource;
        if (values == nil) {
            values = [[NSMutableArray alloc] initWithCapacity:DEFAULT_VOTE_TYPES_COUNT];
            components = [[NSMutableArray alloc] initWithCapacity:DEFAULT_VOTE_TYPES_COUNT];
            for (int i=0; i < DEFAULT_VOTE_TYPES_COUNT; i++) {
                NSInteger value = [dataSource voteResultView:self voteCountsForIndex:i];
                totalVotes += value;
                if (value > maxValue) {
                    maxIndex = i;
                    maxValue = value;
                }
                [values addObject:[NSNumber numberWithInt:value]];
                [components addObject:[PieComponent pieComponentWithValue:value color:colors[i]]];
            }
        }
    }
    
    CGContextRef cgctx = UIGraphicsGetCurrentContext();
    CGContextRetain(cgctx);
    CGContextSaveGState(cgctx);
    
    [self drawList:cgctx];
    [self drawPraise:cgctx];
    [self drawGraph:cgctx];
    
    CGContextRestoreGState(cgctx);
    CGContextRelease(cgctx);
}

// Draw list part of vote result by using titles, icons and values
- (void)drawList:(CGContextRef)cgctx
{
#define OUTER_FRAME_X 10
    
#define INTERNAL_REXT_X (OUTER_FRAME_X + 6)
#define INTERNAL_RECT_Y 8
#define INTERNAL_RECT_WIDTH 144
#define INTERNAL_RECT_HEIGHT 165
    
#define ICON_X (INTERNAL_REXT_X + 9)
#define ICON_Y 18
#define ICON_Y_DELTA 25
    
#define TITLE_X (INTERNAL_REXT_X + 35)
#define TITLE_Y 21
#define TITLE_Y_DELTA 25
#define TITLE_FONT_SIZE 12
    
    // draw background box
    [[UIColor blackColor] setFill];
    CGContextSetAlpha(cgctx, 0.4f);
    CGContextFillRect(cgctx, CGRectMake(INTERNAL_REXT_X, INTERNAL_RECT_Y, INTERNAL_RECT_WIDTH, INTERNAL_RECT_HEIGHT));
    CGContextSetAlpha(cgctx, 1.0f);
    
    // draw icons, label text and voting resule value
    [[UIColor whiteColor] setFill];
    UIFont *normalFont = CS_FONT(TITLE_FONT_SIZE);
    UIFont *boldFont = CS_BOLD_FONT(TITLE_FONT_SIZE);
    
    for (NSInteger i=0; i < DEFAULT_VOTE_TYPES_COUNT; i++) {
        UIImage *icon = [icons objectAtIndex:i];
        [icon drawAtPoint:CGPointMake(ICON_X, ICON_Y + ICON_Y_DELTA * i) blendMode:kCGBlendModeNormal alpha:1.0f];
        
        NSString *title = [titles objectAtIndex:i];
        [title drawAtPoint:CGPointMake(TITLE_X, TITLE_Y + TITLE_Y_DELTA * i) withFont:(i == maxIndex ? boldFont:normalFont)];
        
        NSNumber *value = [values objectAtIndex:i];
        NSString *percentage = [NSString stringWithFormat:@"%.1f%%", ([value floatValue] / totalVotes * 100.0f)];
        CGRect percentageRect = CGRectMake(OUTER_FRAME_X, TITLE_Y + TITLE_Y_DELTA * i, INTERNAL_RECT_WIDTH, TITLE_Y_DELTA);
        [percentage drawInRect:percentageRect
                      withFont:(i == maxIndex ? boldFont:normalFont)
                 lineBreakMode:NSLineBreakByWordWrapping
                     alignment:NSTextAlignmentRight];
    }
}


// Draw praise text portion of vote result
- (void)drawPraise:(CGContextRef)cgctx
{
#define PRAISE_TEXT_X (OUTER_FRAME_X + 149)
#define PRAISE_TEXT1_Y 14
#define PRAISE_TEXT2_Y 28
#define PRAISE_TEXT_W 131
    
#define PRAISE_TEXT1_SIZE 13
#define PRAISE_TEXT2_SIZE 22
  
    const CGRect praise1Rect = CGRectMake(PRAISE_TEXT_X, PRAISE_TEXT1_Y, PRAISE_TEXT_W, PRAISE_TEXT1_SIZE);
    const CGRect praise2Rect = CGRectMake(PRAISE_TEXT_X, PRAISE_TEXT2_Y, PRAISE_TEXT_W, PRAISE_TEXT2_SIZE);
    UIFont *praise1Font = CS_FONT(PRAISE_TEXT1_SIZE);
    UIFont *praise2Font = CS_BOLD_FONT(PRAISE_TEXT2_SIZE);
    
    [[UIColor whiteColor] setFill];
    
    if (_style == VoteResultViewForMale) {
        [@"He' so" drawInRect:praise1Rect withFont:praise1Font lineBreakMode:NSLineBreakByWordWrapping alignment:UITextAlignmentCenter];

    } else {
        [@"She's so" drawInRect:praise1Rect withFont:praise1Font lineBreakMode:NSLineBreakByWordWrapping alignment:UITextAlignmentCenter];
    }

    NSString *praise = [titles objectAtIndex:maxIndex];
    [praise drawInRect:praise2Rect withFont:praise2Font lineBreakMode:NSLineBreakByWordWrapping alignment:UITextAlignmentCenter];
}

// draw pie graph porting by using voting result values
// NOTE: Parts of code copied from PCPieChart open source and modified for my own purpose
- (void)drawGraph:(CGContextRef)cgctx
{
#define CHART_BACKGROUND_X (OUTER_FRAME_X + 162)
#define CHART_BACKGOURND_Y 62
    
#define CHART_X (CHART_BACKGROUND_X + 9)
#define CHART_Y (CHART_BACKGOURND_Y + 9)
#define CHART_W 124
#define CHART_H 124
    
    const float margin = 15;
    if (diameter == 0) {
        diameter = MIN(CHART_W, CHART_H) - 2 * margin;
    }
    const float x = CHART_X;
    const float y = CHART_Y;
    const float inner_radius = diameter/2;
    const float origin_x = x + diameter/2;
    const float origin_y = y + diameter/2;
    
    if ([components count] > 0) {
        float total = 0;
        for (PieComponent *component in components) {
            total += component.value;
        }
       	
		float nextStartDeg = 0;
		float endDeg = 0;
		NSMutableArray *tmpComponents = [NSMutableArray array];
		int last_insert = -1;
		for (int i=0; i < [components count]; i++) {
			PieComponent *component  = [components objectAtIndex:i];
			float perc = [component value]/total;
			endDeg = nextStartDeg+perc*360;
			
			CGContextSetFillColorWithColor(cgctx, [component.color CGColor]);
			CGContextMoveToPoint(cgctx, origin_x, origin_y);
			CGContextAddArc(cgctx, origin_x, origin_y, inner_radius, (nextStartDeg-90)*M_PI/180.0, (endDeg-90)*M_PI/180.0, 0);
			CGContextClosePath(cgctx);
			CGContextFillPath(cgctx);
			
			[component setStartDeg:nextStartDeg];
			[component setEndDeg:endDeg];
			if (nextStartDeg < 180) {
				[tmpComponents addObject:component];
			} else {
				if (last_insert == -1) {
					last_insert = i;
					[tmpComponents addObject:component];
				} else {
					[tmpComponents insertObject:component atIndex:last_insert];
				}
			}
			nextStartDeg = endDeg;
		}
    }
    
    [chartBackground drawAtPoint:CGPointMake(CHART_BACKGROUND_X, CHART_BACKGOURND_Y)];
}

@end
