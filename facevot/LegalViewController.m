//
//  LegalViewController.m
//  facevot
//
//  Created by 태한 김 on 12. 8. 1..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//

#import "LegalViewController.h"

@implementation LegalViewController

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
        self.title = @"Terms of Use";

        UITextView *txtView = [[UITextView alloc] initWithFrame:CGRectMake(5, 0, 310, [UIScreen mainScreen].bounds.size.height-65)];
        [txtView setTextColor:[UIColor darkGrayColor]];
        [txtView setEditable:NO];

        [self.view addSubview:txtView];
        [self.tableView setSeparatorColor:[UIColor clearColor]];
        [self.tableView setScrollEnabled:NO];

        [txtView setText:@"Basic Terms\n\n\
         You must be 13 years or older to use this site.\n\
         You may not post nude, partially nude, or sexually suggestive photos.\n\
         You are responsible for any activity that occurs under your screen name.\n\
         You are responsible for keeping your password secure.\n\
         You must not abuse, harass, threaten, impersonate or intimidate other Instagram users.\n\
         You may not use the Instagram service for any illegal or unauthorized purpose. International users agree to comply with all local laws regarding online conduct and acceptable content.\n\
         You are solely responsible for your conduct and any data, text, information, screen names, graphics, photos, profiles, audio and video clips, links (\"Content\") that you submit, post, and display on the Instagram service.\n\
         You must not modify, adapt or hack Instagram or modify another website so as to falsely imply that it is associated with Instagram.\n\
         You must not access Instagram's private API by any other means other than the Instagram application itself.\n\
         You must not crawl, scrape, or otherwise cache any content from Instagram including but not limited to user profiles and photos.\n\
         You must not create or submit unwanted email or comments to any Instagram members (Spam).\n\
         You must not use web URLs in your name without prior written consent from Instagram, inc.\n\
         You must not transmit any worms or viruses or any code of a destructive nature.\n\
         You must not, in the use of Instagram, violate any laws in your jurisdiction (including but not limited to copyright laws).\n\
         Violation of any of these agreements will result in the termination of your Instagram account. While Instagram prohibits such conduct and content on its site, you understand and agree that Instagram cannot be responsible for the Content posted on its web site and you nonetheless may be exposed to such materials and that you use the Instagram service at your own risk.\n\n\
         General Conditions\n\n\
         We reserve the right to modify or terminate the Instagram service for any reason, without notice at any time.\n\
         We reserve the right to alter these Terms of Use at any time. If the alterations constitute a material change to the Terms of Use, we will notify you via internet mail according to the preference expressed on your account. What constitutes a \"material change\" will be determined at our sole discretion, in good faith and using common sense and reasonable judgement.\n\
         We reserve the right to refuse service to anyone for any reason at any time.\n\
         We reserve the right to force forfeiture of any username that becomes inactive, violates trademark, or may mislead other users.\n\
         We may, but have no obligation to, remove Content and accounts containing Content that we determine in our sole discretion are unlawful, offensive, threatening, libelous, defamatory, obscene or otherwise objectionable or violates any party's intellectual property or these Terms of Use.\n\
         We reserve the right to reclaim usernames on behalf of businesses or individuals that hold legal claim or trademark on those usernames.\n\n\
         Proprietary Rights in Content on Instagram\n\n\
         Instagram does NOT claim ANY ownership rights in the text, files, images, photos, video, sounds, musical works, works of authorship, applications, or any other materials (collectively, \"Content\") that you post on or through the Instagram Services. By displaying or publishing (\"posting\") any Content on or through the Instagram Services, you hereby grant to Instagram a non-exclusive, fully paid and royalty-free, worldwide, limited license to use, modify, delete from, add to, publicly perform, publicly display, reproduce and translate such Content, including without limitation distributing part or all of the Site in any media formats through any media channels, except Content not shared publicly (\"private\") will not be distributed outside the Instagram Services.\n\
         Some of the Instagram Services are supported by advertising revenue and may display advertisements and promotions, and you hereby agree that Instagram may place such advertising and promotions on the Instagram Services or on, about, or in conjunction with your Content. The manner, mode and extent of such advertising and promotions are subject to change without specific notice to you.\n\
         You represent and warrant that: (i) you own the Content posted by you on or through the Instagram Services or otherwise have the right to grant the license set forth in this section, (ii) the posting and use of your Content on or through the Instagram Services does not violate the privacy rights, publicity rights, copyrights, contract rights, intellectual property rights or any other rights of any person, and (iii) the posting of your Content on the Site does not result in a breach of contract between you and a third party. You agree to pay for all royalties, fees, and any other monies owing any person by reason of Content you post on or through the Instagram Services.\n\
         The Instagram Services contain Content of Instagram (\"Instagram Content\"). Instagram Content is protected by copyright, trademark, patent, trade secret and other laws, and Instagram owns and retains all rights in the Instagram Content and the Instagram Services. Instagram hereby grants you a limited, revocable, nonsublicensable license to reproduce and display the Instagram Content (excluding any software code) solely for your personal use in connection with viewing the Site and using the Instagram Services.\n\
         The Instagram Services contain Content of Users and other Instagram licensors. Except as provided within this Agreement, you may not copy, modify, translate, publish, broadcast, transmit, distribute, perform, display, or sell any Content appearing on or through the Instagram Services.\n\
         Instagram performs technical functions necessary to offer the Instagram Services, including but not limited to transcoding and/or reformatting Content to allow its use throughout the Instagram Services.\n\
         Although the Site and other Instagram Services are normally available, there will be occasions when the Site or other Instagram Services will be interrupted for scheduled maintenance or upgrades, for emergency repairs, or due to failure of telecommunications links and equipment that are beyond the control of Instagram. Also, although Instagram will normally only delete Content that violates this Agreement, Instagram reserves the right to delete any Content for any reason, without prior notice. Deleted content may be stored by Instagram in order to comply with certain legal obligations and is not retrievable without a valid court order. Consequently, Instagram encourages you to maintain your own backup of your Content. In other words, Instagram is not a backup service. Instagram will not be liable to you for any modification, suspension, or discontinuation of the Instagram Services, or the loss of any Content."];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
