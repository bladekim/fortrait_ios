//
//  CSFBThumbCell.m
//  facevot
//
//  Created by 태한 김 on 12. 8. 14..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//

#import "CSFBThumbCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation CSFBThumbCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        imgArray = [[NSMutableArray alloc] initWithCapacity:4];

        for( NSUInteger i = 0; i < 4; i ++ )
        {
            UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake(THUMB_CELL_H*i, 0, THUMB_CELL_H, THUMB_CELL_H)];
            [imgV setClipsToBounds:YES];
            [imgV setContentMode:UIViewContentModeScaleAspectFill];
            imgV.layer.borderColor = [UIColor blackColor].CGColor;
            imgV.layer.borderWidth = 1.0f;

            UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, THUMB_CELL_H, THUMB_CELL_H)];
            [btn setTag:i];
            [btn addTarget:self action:@selector(imageTap:) forControlEvents:UIControlEventTouchUpInside];
            [imgV addSubview:btn];
            [imgV setUserInteractionEnabled:YES];

            [imgArray addObject:imgV];
            [imgV setHidden:YES];
            [self.contentView addSubview:imgV];
        }
    }
    return self;
}

- (void) setImageURL:(NSString*)urlStr atIndex:(NSUInteger)idx
{
    if( 4 <= idx ) return;

    UIImageView *theView = imgArray[idx];

    [theView setHidden:NO];
    [theView setImageWithURL:[NSURL URLWithString:urlStr]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
//    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

// 이 셀에 미리 설정된 셀 인덱스 값 * 4 + 섬네일 태그 값 을 딜레게이트를 통해 전해 줌.
- (void) imageTap:(UIButton*) sender
{
    if( self.delegate != nil && [self.delegate respondsToSelector:@selector(selectedFBCell:)] )
        [self.delegate selectedFBCell: _index*4+sender.tag ];
}

@end
