//
//  CSFBAlbumCell.h
//  facevot
//
//  Created by 김태한 on 12. 6. 30..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CSFBAlbumCell : UITableViewCell
{
    UIImageView *imgView;
    UILabel *nameLabel;
}

-(void) setImageURL:(NSString*)url name:(NSString*)name;

@end