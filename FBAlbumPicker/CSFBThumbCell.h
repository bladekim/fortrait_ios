//
//  CSFBThumbCell.h
//  facevot
//
//  Created by 태한 김 on 12. 8. 14..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CSFBThumbCell : UITableViewCell
{
    NSMutableArray *imgArray;
}

@property (nonatomic, assign) id delegate;
@property (nonatomic, assign) NSUInteger index;

- (void) setImageURL:(NSString*)urlStr atIndex:(NSUInteger)idx;

@end

@protocol FBCellDelegate

- (void) selectedFBCell:(NSUInteger) idx;

@end