//
//  CSFBThumbViewController.m
//  facevot
//
//  Created by 태한 김 on 12. 8. 14..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//
/*
 "data": [
 {
 "id": "10150342830529409",
 "from": {
 "name": "Taehan Kim",
 "id": "777739408"
 },
 "picture": "https://fbcdn-photos-a.akamaihd.net/hphotos-ak-snc6/250521_10150342830529409_522119_s.jpg",
 "source": "https://fbcdn-sphotos-a-a.akamaihd.net/hphotos-ak-snc6/250521_10150342830529409_522119_n.jpg",
 "height": 720,
 "width": 540,
 "images": [
 {
 "height": 2048,
 "width": 1536,
 "source": "https://fbcdn-sphotos-a-a.akamaihd.net/hphotos-ak-snc6/244098_10150342830529409_522119_o.jpg"
 },
 {
 "height": 720,
 "width": 540,
 "source": "https://fbcdn-sphotos-a-a.akamaihd.net/hphotos-ak-snc6/250521_10150342830529409_522119_n.jpg"
 },
 {
 "height": 720,
 "width": 540,
 "source": "https://fbcdn-sphotos-a-a.akamaihd.net/hphotos-ak-snc6/250521_10150342830529409_522119_n.jpg"
 },
 {
 "height": 480,
 "width": 360,
 "source": "https://fbcdn-sphotos-a-a.akamaihd.net/hphotos-ak-snc6/s480x480/250521_10150342830529409_522119_n.jpg"
 },
 {
 "height": 320,
 "width": 240,
 "source": "https://fbcdn-sphotos-a-a.akamaihd.net/hphotos-ak-snc6/s320x320/250521_10150342830529409_522119_n.jpg"
 },
 {
 "height": 240,
 "width": 180,
 "source": "https://fbcdn-photos-a.akamaihd.net/hphotos-ak-snc6/250521_10150342830529409_522119_a.jpg"
 },
 {
 "height": 130,
 "width": 97,
 "source": "https://fbcdn-photos-a.akamaihd.net/hphotos-ak-snc6/250521_10150342830529409_522119_s.jpg"
 },
 {
 "height": 130,
 "width": 97,
 "source": "https://fbcdn-photos-a.akamaihd.net/hphotos-ak-snc6/s75x225/250521_10150342830529409_522119_s.jpg"
 }
 ],
 "link": "https://www.facebook.com/photo.php?fbid=10150342830529409&set=a.213750259408.164202.777739408&type=1",
 "icon": "https://s-static.ak.facebook.com/rsrc.php/v2/yz/r/StEh3RhPvjk.gif",
 "created_time": "2011-06-11T14:26:45+0000",
 "position": 1,
 "updated_time": "2011-06-11T14:26:46+0000",
 "comments": {
 "data": [
 {
 "id": "10150342830529409_6000464",
 "from": {
 "name": "Taehan Kim",
 "id": "777739408"
 },
 "message": "11\ubc88 \uc120\uc218\ub4e4. \uc0ac\uc2e4 \ub098\ub3c4 \uac19\uc740 \uc637. \u314b",
 "can_remove": true,
 "created_time": "2011-06-11T14:27:37+0000",
 "like_count": 0,
 "user_likes": false
 }
 ],
 "paging": {
 "next": "https://graph.facebook.com/10150342830529409/comments?access_token=AAAAAAITEghMBAK1KYv1QtCs6hqUiwhGO7vNQCeyWztnuEZC8RG98nshst6XBbh0r4XziDTf1p75MDldZANAwvIFwG0hy8XWLnA38vT5QZDZD&limit=25&offset=25&__after_id=10150342830529409_6000464"
 }
 },
 "likes": {
 "data": [
 {
 "id": "100001018768101",
 "name": "\uc131\uc724\uae30"
 },
 {
 "id": "100000656748520",
 "name": "Euisang Jung"
 }
 ],
 "paging": {
 "next": "https://graph.facebook.com/10150342830529409/likes?access_token=AAAAAAITEghMBAK1KYv1QtCs6hqUiwhGO7vNQCeyWztnuEZC8RG98nshst6XBbh0r4XziDTf1p75MDldZANAwvIFwG0hy8XWLnA38vT5QZDZD&limit=25&offset=25&__after_id=100000656748520"
 }
 }
 },
 */
#import "CSFBThumbViewController.h"
#import "CSFBThumbCell.h"
#import "YBSwipeViewController.h"
#import "AppDelegate.h"

@implementation CSFBThumbViewController

- (id)initWithStyle:(UITableViewStyle)style title:(NSString*)title albumId:(NSString*)theId
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        self.title = title;
        self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.backgroundColor = THEME_COLOR;
        self.tableView.separatorColor = THEME_COLOR;

        [self.tableView registerClass:[CSFBThumbCell class] forCellReuseIdentifier:@"FBthumbCell"];
    }

    START_WAIT_VIEW;
    FBRequest *req = [FBRequest requestForGraphPath:[NSString stringWithFormat:@"%@/photos",theId]];
    [req startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error)
    {
//    [FBRequest startWithGraphPath:[NSString stringWithFormat:@"%@/photos",theId]
//                completionHandler:^(FBRequestConnection *connection, NSDictionary *result, NSError *error)
//     {
         picArray = [result objectForKey:@"data"];

         dispatch_async(dispatch_get_main_queue(), ^(){
             STOP_WAIT_VIEW;
             [self.tableView reloadData];
         });
     }];

    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;

    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSUInteger tailNum = 0;
    if( 0 < [picArray count] ) tailNum = 1;
    return( [picArray count] / 4 ) + tailNum;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return THUMB_CELL_H;
}

- (UITableViewCell *)tableView:(UITableView *)tView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CSFBThumbCell *cell = [tView dequeueReusableCellWithIdentifier:@"FBthumbCell" forIndexPath:indexPath];

    NSUInteger cidx = indexPath.row * 4;

    [cell setDelegate:self];
    [cell setIndex:indexPath.row];

    for(NSUInteger step = 0 ; step < 4 && (cidx+step) < [picArray count]; step ++ )
    {
        NSArray *imgs = ((NSDictionary*)picArray[cidx + step])[@"images"];
        [cell setImageURL:imgs[5][@"source"] atIndex:step];
    }

    return cell;
}

#pragma mark - CSFB delegate

- (void) selectedFBCell:(NSUInteger) index
{
    NSURL *imgUrl = [NSURL URLWithString:picArray[index][@"source"]];
    dispatch_async(dispatch_get_main_queue(), ^{
        START_WAIT_VIEW;
    });

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^
    {
        NSData *imgData = [NSData dataWithContentsOfURL:imgUrl];

        UIImage *img = [UIImage imageWithData:imgData];

        dispatch_async(dispatch_get_main_queue(), ^{
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            YBSwipeViewController *swipeViewCntrlr = (YBSwipeViewController *)[[appDelegate window] rootViewController];
            [(id<UIImagePickerControllerDelegate>)[(UINavigationController*)swipeViewCntrlr.mainViewCntrlr topViewController] imagePickerController:(UIImagePickerController*)self didFinishPickingImage:img editingInfo:nil];
        });
    });
}

@end
