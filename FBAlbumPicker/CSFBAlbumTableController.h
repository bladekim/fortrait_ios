//
//  CSFBAlbumTableController.h
//  facevot
//
//  Created by 김태한 on 12. 8. 14..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//
// NOTE: 언제나 페이스북에 로그인 된 상태에서만 사용된다는 가정.

#import <UIKit/UIKit.h>

@interface CSFBAlbumTableController : UITableViewController<UITableViewDataSource>
{
    NSMutableArray *albumList;
}

@end
